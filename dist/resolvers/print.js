'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.printResolvers = exports.printTypeDefs = exports.printaFour = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _nodePrinter = require('node-printer');

var _nodePrinter2 = _interopRequireDefault(_nodePrinter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var thermalEpsonOptions = {
  media: 'Custom.82x200mm',
  cpi: 12,
  lpi: 6,
  'page-left': 15,
  'page-top': 10,
  'page-bottom': 30
};

// Available Printers
var printers = _nodePrinter2.default.list();
var printer = void 0;

if (printers.length === 0) {
  console.log('Error: No Printers Installed');
} else {
  printer = new _nodePrinter2.default(printers[0]);
}

var printaFour = exports.printaFour = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(file) {
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(printers.length > 0)) {
              _context.next = 4;
              break;
            }

            _context.next = 3;
            return printer.printFile(file);

          case 3:
            return _context.abrupt('return', 0);

          case 4:
            return _context.abrupt('return', -1);

          case 5:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function printaFour(_x) {
    return _ref.apply(this, arguments);
  };
}();

var printTypeDefs = exports.printTypeDefs = {
  output: 'type PRINT{\n    id: Int\n    error: String\n  }\n  type PRINTCANCEL{\n    status: Int\n    error: String\n  }',
  query: ['print (printString: String): PRINT'],
  mutation: ['cancelPrint (id: Int): PRINTCANCEL']
};

var printResolvers = exports.printResolvers = {
  Query: {
    print: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(root, _ref3) {
        var printString = _ref3.printString;
        var job;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(printers.length === 0)) {
                  _context2.next = 2;
                  break;
                }

                return _context2.abrupt('return', { error: 'Printer Not Found' });

              case 2:
                _context2.next = 4;
                return printer.printText(Buffer.from(printString, 'base64').toString('utf8'), thermalEpsonOptions);

              case 4:
                job = _context2.sent;
                return _context2.abrupt('return', { id: job.identifier });

              case 6:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, undefined);
      }));

      return function print(_x2, _x3) {
        return _ref2.apply(this, arguments);
      };
    }()
  },
  Mutation: {
    cancelPrint: function () {
      var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(root, _ref5) {
        var id = _ref5.id;
        var status;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (!(printers.length === 0)) {
                  _context3.next = 2;
                  break;
                }

                return _context3.abrupt('return', { error: 'Printer Not Found' });

              case 2:
                status = 0;

                printer.jobs.forEach(function (job) {
                  if (job.identifier === id) {
                    job.cancel();
                    status = 1;
                  }
                });
                return _context3.abrupt('return', { status: status });

              case 5:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, undefined);
      }));

      return function cancelPrint(_x4, _x5) {
        return _ref4.apply(this, arguments);
      };
    }()
  }
};