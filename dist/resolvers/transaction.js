'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transactionResolvers = exports.transactionTypeDefs = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongooseToGraphql = require('mongoose-to-graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _transaction = require('../models/transaction');

var _transaction2 = _interopRequireDefault(_transaction);

var _currentState = require('../models/currentState');

var _currentState2 = _interopRequireDefault(_currentState);

var _helper = require('../helper');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TransactionGQLSchema = (0, _mongooseToGraphql.modelToType)(_transaction2.default);
/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

var TransactionGQLInputSchema = (0, _mongooseToGraphql.modelToCreateType)(_transaction2.default) + '\n' + (0, _mongooseToGraphql.modelToUpdateType)(_transaction2.default);

var transactionTypeDefs = exports.transactionTypeDefs = {
  output: TransactionGQLSchema,
  input: TransactionGQLInputSchema,
  query: ['getTransactions (transaction: TransactionUpdate, limit: Int, skip: Int): [Transaction]'],
  mutation: ['createTransaction(transaction: TransactionCreate): Transaction', 'updateTransaction(filter: TransactionUpdate, transaction: TransactionCreate, isRefund: Int): Transaction']
};

var calculateAverage = function calculateAverage(currentStock) {
  var keepAsPercent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  var products = currentStock.products.map(function (product) {
    var reqProduct = _lodash2.default.pick(product, ['_id', 'id', 'name', 'quantity', 'basePrice', 'netPrice', 'tax', 'discount', 'total', 'batch', 'costPrice', 'sellPrice', 'hsn']);
    reqProduct.discountVal = product.basePrice * (product.discount / 10000);
    reqProduct.taxVal = product.basePrice * (1 - product.discount / 10000) * (product.tax / 10000);
    // eslint-disable-next-line no-param-reassign
    product.total = product.netPrice * product.quantity;
    if (!keepAsPercent) {
      reqProduct.discount = reqProduct.discountVal;
      reqProduct.tax = reqProduct.taxVal;
    }
    return [reqProduct];
  }).reduce(function (acc, nxt) {
    var index = acc.map(function (p) {
      return p.id.toString();
    }).indexOf(nxt[0].id.toString());
    if (index > -1) {
      var newProduct = _lodash2.default.cloneDeep(nxt[0]);
      // eslint-disable-next-line no-param-reassign
      nxt[0].total = nxt[0].netPrice * nxt[0].quantity;
      newProduct.quantity = acc[index].quantity + nxt[0].quantity;
      newProduct.basePrice = (acc[index].basePrice * acc[index].quantity + nxt[0].basePrice * nxt[0].quantity) / newProduct.quantity;
      newProduct.netPrice = (acc[index].netPrice * acc[index].quantity + nxt[0].netPrice * nxt[0].quantity) / newProduct.quantity;
      newProduct.tax = (acc[index].taxVal * acc[index].quantity + nxt[0].taxVal * nxt[0].quantity) / newProduct.quantity;
      newProduct.discount = (acc[index].discountVal * acc[index].quantity + nxt[0].discountVal * nxt[0].quantity) / newProduct.quantity;
      newProduct.total = acc[index].total + nxt[0].total;
      acc[index] = newProduct;
    } else {
      // eslint-disable-next-line no-param-reassign
      nxt[0].total = nxt[0].netPrice * nxt[0].quantity;
      acc.push(nxt[0]);
    }
    return acc;
  });
  var price = parseInt(products.map(function (p) {
    return p.basePrice * p.quantity;
  }).reduce(function (acc, nxt) {
    return acc + nxt;
  }), 10);
  var tax = parseInt(products.map(function (p) {
    return p.tax * p.quantity;
  }).reduce(function (acc, nxt) {
    return acc + nxt;
  }), 10);
  var discount = parseInt(products.map(function (p) {
    return p.discount * p.quantity;
  }).reduce(function (acc, nxt) {
    return acc + nxt;
  }), 10);
  var total = parseInt(products.map(function (p) {
    return p.netPrice * p.quantity;
  }).reduce(function (acc, nxt) {
    return acc + nxt;
  }), 10);
  var recalculatedCurrentStock = {
    products: products,
    price: price,
    discount: discount,
    tax: tax,
    total: total
  };
  return recalculatedCurrentStock;
};

var createNewCurrentStateTotal = function createNewCurrentStateTotal(newCurrentStateStocks) {
  var tempCurrentStateStocks = JSON.parse(JSON.stringify(newCurrentStateStocks));
  var newCurrentStateTotalStocks = tempCurrentStateStocks.reduce(function (acc, nxt) {
    acc.products = _lodash2.default.concat(acc.products, nxt.products);
    return acc;
  });
  var recalculatedTotal = calculateAverage(newCurrentStateTotalStocks);
  return recalculatedTotal;
};

var postStock = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(transaction, currentState) {
    var newCurrentState, currStockLast;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            newCurrentState = {
              stocks: [],
              total: null
            };

            // Change newCurrentState.stocks

            if (currentState) {
              newCurrentState = _lodash2.default.pick(JSON.parse(JSON.stringify(currentState)), ['stocks', 'total']);
            }
            currStockLast = _lodash2.default.pick(transaction, ['products', 'price', 'tax', 'total']);

            currStockLast.stock = transaction._id;
            newCurrentState.stocks.unshift(currStockLast);
            // Change newCurrentState.total
            newCurrentState.total = createNewCurrentStateTotal(newCurrentState.stocks);

            _context.next = 8;
            return _currentState2.default.findOneAndUpdate({}, newCurrentState, { upsert: true });

          case 8:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function postStock(_x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

var postOrder = function () {
  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(transaction, currentState) {
    var tempCurrentState, soldProducts, quantityArr, quantitySum, batchIDs, done, s, p, batchID, idx, tempStock, newCurrentState;
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            // Change newCurrentState.stocks
            tempCurrentState = _lodash2.default.pick(JSON.parse(JSON.stringify(currentState)), ['stocks', 'total']);
            soldProducts = transaction.products;
            quantityArr = soldProducts.map(function (p) {
              return p.quantity;
            });
            quantitySum = quantityArr.reduce(function (acc, nxt) {
              return acc + nxt;
            }, 0);
            batchIDs = soldProducts.map(function (p) {
              return p._id.toString();
            });
            done = false;
            s = 0;

          case 7:
            if (!(s < tempCurrentState.stocks.length)) {
              _context2.next = 31;
              break;
            }

            p = 0;

          case 9:
            if (!(p < tempCurrentState.stocks[s].products.length)) {
              _context2.next = 24;
              break;
            }

            if (!(quantitySum === 0)) {
              _context2.next = 13;
              break;
            }

            done = true;
            return _context2.abrupt('break', 24);

          case 13:
            batchID = tempCurrentState.stocks[s].products[p]._id.toString();
            idx = batchIDs.indexOf(batchID);

            if (!(idx > -1)) {
              _context2.next = 21;
              break;
            }

            if (!(tempCurrentState.stocks[s].products[p].quantity < quantityArr[idx])) {
              _context2.next = 18;
              break;
            }

            throw new Error('Transaction has already been made somewhere else.');

          case 18:
            tempCurrentState.stocks[s].products[p].quantity -= quantityArr[idx];
            quantitySum -= quantityArr[idx];
            quantityArr[idx] = 0;

          case 21:
            p += 1;
            _context2.next = 9;
            break;

          case 24:
            tempCurrentState.stocks[s].products = tempCurrentState.stocks[s].products.filter(function (p) {
              return p.quantity > 0;
            });
            if (tempCurrentState.stocks[s].products.length > 0) {
              tempStock = calculateAverage(tempCurrentState.stocks[s]);

              tempCurrentState.stocks[s].price = tempStock.price;
              tempCurrentState.stocks[s].discount = tempStock.discount;
              tempCurrentState.stocks[s].tax = tempStock.tax;
              tempCurrentState.stocks[s].total = tempStock.total;
            }

            if (!done) {
              _context2.next = 28;
              break;
            }

            return _context2.abrupt('break', 31);

          case 28:
            s += 1;
            _context2.next = 7;
            break;

          case 31:
            if (!(quantitySum > 0)) {
              _context2.next = 33;
              break;
            }

            throw new Error('Transaction has already been made somewhere else.');

          case 33:
            newCurrentState = {};

            newCurrentState.stocks = tempCurrentState.stocks.filter(function (s) {
              return s.products.length > 0;
            });
            // Change total

            if (!(newCurrentState.stocks.length > 0)) {
              _context2.next = 41;
              break;
            }

            newCurrentState.total = createNewCurrentStateTotal(newCurrentState.stocks);
            _context2.next = 39;
            return _currentState2.default.findOneAndUpdate({}, newCurrentState, { upsert: true });

          case 39:
            _context2.next = 44;
            break;

          case 41:
            newCurrentState = {};
            _context2.next = 44;
            return _currentState2.default.findOneAndRemove();

          case 44:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined);
  }));

  return function postOrder(_x4, _x5) {
    return _ref2.apply(this, arguments);
  };
}();

var transactionResolvers = exports.transactionResolvers = {
  Query: {
    getTransactions: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(root, _ref4) {
        var transaction = _ref4.transaction,
            limit = _ref4.limit,
            skip = _ref4.skip;
        var dTransaction, validTransactions;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                dTransaction = (0, _helper.dollarify)(transaction);
                _context3.next = 3;
                return _transaction2.default.find(dTransaction).sort({ createdAt: -1 }).skip(skip || 0).limit(limit || Number.MAX_SAFE_INTEGER);

              case 3:
                validTransactions = _context3.sent;
                return _context3.abrupt('return', validTransactions.map(_helper.prepare));

              case 5:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, undefined);
      }));

      return function getTransactions(_x6, _x7) {
        return _ref3.apply(this, arguments);
      };
    }()
  },
  Mutation: {
    createTransaction: function () {
      var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(root, args) {
        var transaction, currentState, savedTransaction;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                // pass it through a validator
                transaction = (0, _transaction2.default)(args.transaction);
                _context4.next = 3;
                return _currentState2.default.findOne().sort({ createdAt: -1 });

              case 3:
                currentState = _context4.sent;
                _context4.next = 6;
                return transaction.save();

              case 6:
                savedTransaction = _context4.sent;

                delete savedTransaction.details;
                _context4.prev = 8;

                if (!(transaction.type === 'stock')) {
                  _context4.next = 14;
                  break;
                }

                _context4.next = 12;
                return postStock(savedTransaction, currentState);

              case 12:
                _context4.next = 16;
                break;

              case 14:
                _context4.next = 16;
                return postOrder(savedTransaction, currentState);

              case 16:
                return _context4.abrupt('return', (0, _helper.prepare)(savedTransaction));

              case 19:
                _context4.prev = 19;
                _context4.t0 = _context4['catch'](8);
                _context4.next = 23;
                return _transaction2.default.findByIdAndRemove(savedTransaction._id);

              case 23:
                throw new Error('Transaction has already been made somewhere else.');

              case 24:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, undefined, [[8, 19]]);
      }));

      return function createTransaction(_x8, _x9) {
        return _ref5.apply(this, arguments);
      };
    }(),
    updateTransaction: function () {
      var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(root, _ref7) {
        var filter = _ref7.filter,
            transaction = _ref7.transaction,
            isRefund = _ref7.isRefund;
        var dFilter, prev, out, currentState, reqOut;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                dFilter = (0, _helper.dollarify)(filter);

                if (!(isRefund === 1)) {
                  _context5.next = 8;
                  break;
                }

                _context5.next = 5;
                return _transaction2.default.findOne(dFilter);

              case 5:
                prev = _context5.sent;

                if (!(prev.subType === 'refund')) {
                  _context5.next = 8;
                  break;
                }

                throw new Error('Refund has already been made somewhere else.');

              case 8:
                _context5.next = 10;
                return _transaction2.default.findOneAndUpdate(dFilter, transaction, { upsert: true });

              case 10:
                out = _context5.sent;
                _context5.next = 13;
                return _transaction2.default.findOne(dFilter);

              case 13:
                out = _context5.sent;

                if (!(isRefund === 1)) {
                  _context5.next = 27;
                  break;
                }

                _context5.next = 17;
                return _currentState2.default.findOne().sort({ createdAt: -1 });

              case 17:
                currentState = _context5.sent;

                /* eslint-disable no-param-reassign */
                out.products = out.products.filter(function (p) {
                  return 'refund' in p && p.refund.keep > 0;
                });

                if (!(out.products.length > 0)) {
                  _context5.next = 27;
                  break;
                }

                out.products.forEach(function (p) {
                  p.netPrice = p.costPrice;
                  p.basePrice = p.costPrice / (1 + p.tax / 10000);
                  p.quantity = p.refund.keep;
                  p._id = _mongoose2.default.Types.ObjectId();
                  p.total = p.costPrice * p.refund.keep;
                  p.batch = p.batch + '_Ref';
                });
                /* eslint-enable no-param-reassign */
                _lodash2.default.extend(out, calculateAverage(out, true));
                reqOut = JSON.parse(JSON.stringify(out));

                delete reqOut.details;

                if (!(transaction.type === 'order')) {
                  _context5.next = 27;
                  break;
                }

                _context5.next = 27;
                return postStock(reqOut, currentState);

              case 27:
                return _context5.abrupt('return', (0, _helper.prepare)(out));

              case 30:
                _context5.prev = 30;
                _context5.t0 = _context5['catch'](0);
                throw _context5.t0;

              case 33:
              case 'end':
                return _context5.stop();
            }
          }
        }, _callee5, undefined, [[0, 30]]);
      }));

      return function updateTransaction(_x10, _x11) {
        return _ref6.apply(this, arguments);
      };
    }()
  }
};