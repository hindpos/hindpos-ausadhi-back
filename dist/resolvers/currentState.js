'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.currentStateResolvers = exports.currentStateTypeDefs = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongooseToGraphql = require('mongoose-to-graphql');

var _currentState = require('../models/currentState');

var _currentState2 = _interopRequireDefault(_currentState);

var _helper = require('../helper');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CurrentStateGQLSchema = (0, _mongooseToGraphql.modelToType)(_currentState2.default);
var CurrentStateGQLInputSchema = (0, _mongooseToGraphql.modelToCreateType)(_currentState2.default) + '\n' + (0, _mongooseToGraphql.modelToUpdateType)(_currentState2.default);

var currentStateTypeDefs = exports.currentStateTypeDefs = {
  output: CurrentStateGQLSchema,
  input: CurrentStateGQLInputSchema,
  query: ['getCurrentState: CurrentState'],
  mutation: ['setCurrentState(currentState: CurrentStateCreate): CurrentState']
};

var currentStateResolvers = exports.currentStateResolvers = {
  Query: {
    getCurrentState: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var currentState;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _currentState2.default.findOne().populate([{
                  path: 'stocks.products.id',
                  select: '_id name basePrice taxes tax discount netPrice costPrice details.prescription'
                }]).populate([{
                  path: 'total.products.id',
                  select: '_id minQuantity maxQuantity'
                }]);

              case 2:
                currentState = _context.sent;
                return _context.abrupt('return', (0, _helper.prepare)(currentState));

              case 4:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, undefined);
      }));

      return function getCurrentState() {
        return _ref.apply(this, arguments);
      };
    }()
  },
  Mutation: {
    setCurrentState: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(root, args) {
        var currentState, out;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                // pass it through a validator
                currentState = (0, _currentState2.default)(args.currentState);
                _context2.prev = 1;
                _context2.next = 4;
                return _currentState2.default.findOneAndUpdate({}, currentState, {
                  upsert: true,
                  setDefaultsOnInsert: true
                });

              case 4:
                out = _context2.sent;
                _context2.next = 7;
                return _currentState2.default.findOne();

              case 7:
                out = _context2.sent;
                return _context2.abrupt('return', (0, _helper.prepare)(out));

              case 11:
                _context2.prev = 11;
                _context2.t0 = _context2['catch'](1);
                throw _context2.t0;

              case 14:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, undefined, [[1, 11]]);
      }));

      return function setCurrentState(_x, _x2) {
        return _ref2.apply(this, arguments);
      };
    }()
  }
};