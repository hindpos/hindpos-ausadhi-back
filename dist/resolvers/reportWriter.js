'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.writeTotalStocks = exports.writePrescribedDrugReports = exports.writeScheduleDrugReports = exports.writeInvoiceSummary = exports.writeGSTR2 = exports.writeGSTR1 = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _fsExtra = require('fs-extra');

var _fsExtra2 = _interopRequireDefault(_fsExtra);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _reportHelper = require('./reportHelper');

var _transaction = require('../models/transaction');

var _transaction2 = _interopRequireDefault(_transaction);

var _product = require('../models/product');

var _product2 = _interopRequireDefault(_product);

var _currentState = require('../models/currentState');

var _currentState2 = _interopRequireDefault(_currentState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var REPORTS_ROOT = _path2.default.join(process.env.HOME, 'REPORTS');

var writeGSTR1 = exports.writeGSTR1 = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(startDate, endDate) {
    var orders, refundedOrders, b2bOrders, b2clOrders, b2csOrders, cndrOrders, cndurOrders, directory, dir;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return _transaction2.default.find({
              createdAt: {
                $gte: startDate,
                $lte: endDate
              },
              type: {
                $eq: 'order'
              },
              subType: {
                $nin: ['discard']
              }
            });

          case 2:
            orders = _context.sent;
            _context.next = 5;
            return _transaction2.default.find({
              refundedAt: {
                $gte: startDate,
                $lte: endDate
              },
              type: {
                $eq: 'order'
              },
              subType: {
                $eq: 'refund'
              }
            });

          case 5:
            refundedOrders = _context.sent;


            // Orders with registered users, with GSTIN
            b2bOrders = orders.filter(function (order) {
              return order.person && order.person.gstin;
            });

            // orders with unregistered users, Inter State and with value greater than 2.5 Lakhs

            b2clOrders = orders.filter(function (order) {
              return !(order.person && order.person.gstin) && order.interState && order.total > 25000000;
            });

            // Orders with unregistered users, rest

            b2csOrders = orders.filter(function (order) {
              return !(order.person && order.person.gstin) && order.total < 25000000;
            });

            // Refunds with Registered users

            cndrOrders = refundedOrders.filter(function (order) {
              return order.person && order.person.gstin;
            });

            // Refunds with unregistered users

            cndurOrders = refundedOrders.filter(function (order) {
              return !(order.person && order.person.gstin);
            });
            directory = 'GSTR1 -- ' + new Date(startDate).toDateString() + ' -- ' + new Date(endDate).toDateString();
            _context.prev = 12;

            if (!_fsExtra2.default.ensureDir(_path2.default.join(REPORTS_ROOT, directory))) {
              _context.next = 16;
              break;
            }

            _context.next = 16;
            return _fsExtra2.default.remove(_path2.default.join(REPORTS_ROOT, directory));

          case 16:
            dir = _path2.default.join(REPORTS_ROOT, directory, 'GSTR1');
            _context.next = 19;
            return _fsExtra2.default.mkdirp(dir);

          case 19:

            _fs2.default.writeFileSync(_path2.default.join(dir, 'b2b.csv'), (0, _reportHelper.generateB2BR1)(b2bOrders));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'b2ba.csv'), (0, _reportHelper.generateB2BR1)(b2bOrders, true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'b2cl.csv'), (0, _reportHelper.generateB2CLR1)(b2clOrders));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'b2cla.csv'), (0, _reportHelper.generateB2CLR1)(b2clOrders, true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'b2cs.csv'), (0, _reportHelper.generateB2CSR1)(b2csOrders));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'b2csa.csv'), (0, _reportHelper.generateB2CSR1)(b2csOrders, true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'cndr.csv'), (0, _reportHelper.generateCNDRR1)(cndrOrders));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'cndra.csv'), (0, _reportHelper.generateCNDRR1)(cndrOrders, true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'cndur.csv'), (0, _reportHelper.generateCNDURR1)(cndurOrders, true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'cndura.csv'), (0, _reportHelper.generateCNDURR1)(cndurOrders, true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'hsn.csv'), (0, _reportHelper.generateHsnSummary)(orders));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'exp.csv'), (0, _reportHelper.generateEXPR1)());
            _fs2.default.writeFileSync(_path2.default.join(dir, 'expa.csv'), (0, _reportHelper.generateEXPR1)(true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'ex.csv'), (0, _reportHelper.generateEXPR1)(true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'expa.csv'), (0, _reportHelper.generateEXPR1)(true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'at.csv'), (0, _reportHelper.generateATorATAJR1)());
            _fs2.default.writeFileSync(_path2.default.join(dir, 'ata.csv'), (0, _reportHelper.generateATorATAJR1)(true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'atadj.csv'), (0, _reportHelper.generateATorATAJR1)());
            _fs2.default.writeFileSync(_path2.default.join(dir, 'atadja.csv'), (0, _reportHelper.generateATorATAJR1)(true));
            _context.next = 43;
            break;

          case 40:
            _context.prev = 40;
            _context.t0 = _context['catch'](12);

            // eslint-disable-next-line
            console.log(_context.t0.message);

          case 43:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[12, 40]]);
  }));

  return function writeGSTR1(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var writeGSTR2 = exports.writeGSTR2 = function () {
  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(startDate, endDate) {
    var stocks, b2bStocks, b2burStocks, directory, dir;
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _transaction2.default.find({
              createdAt: {
                $gte: startDate,
                $lte: endDate
              },
              type: {
                $eq: 'stock'
              }
            });

          case 2:
            stocks = _context2.sent;


            // Orders with registered users, with GSTIN
            b2bStocks = stocks.filter(function (stock) {
              return stock.person && stock.person.gstin;
            });

            // orders with unregistered users

            b2burStocks = stocks.filter(function (stock) {
              return !(stock.person && stock.person.gstin);
            });
            directory = 'GSTR2 -- ' + new Date(startDate).toDateString() + ' -- ' + new Date(endDate).toDateString();
            _context2.prev = 6;

            if (!_fsExtra2.default.ensureDir(_path2.default.join(REPORTS_ROOT, directory))) {
              _context2.next = 10;
              break;
            }

            _context2.next = 10;
            return _fsExtra2.default.remove(_path2.default.join(REPORTS_ROOT, directory));

          case 10:
            dir = _path2.default.join(_path2.default.join(REPORTS_ROOT, directory, 'GSTR2'));
            _context2.next = 13;
            return _fsExtra2.default.mkdirp(dir);

          case 13:

            _fs2.default.writeFileSync(_path2.default.join(dir, 'b2b.csv'), (0, _reportHelper.generateB2BorB2BURR2)(b2bStocks, true));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'b2bur.csv'), (0, _reportHelper.generateB2BR1)(b2burStocks));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'impg.csv'), (0, _reportHelper.generateIMPGR2)());
            _fs2.default.writeFileSync(_path2.default.join(dir, 'imps.csv'), (0, _reportHelper.generateIMPSR2)());
            _fs2.default.writeFileSync(_path2.default.join(dir, 'at.csv'), (0, _reportHelper.generateATorATAJR1)());
            _fs2.default.writeFileSync(_path2.default.join(dir, 'atadj.csv'), (0, _reportHelper.generateATorATAJR1)());
            _fs2.default.writeFileSync(_path2.default.join(dir, 'hsnsum.csv'), (0, _reportHelper.generateHsnSummary)(stocks));
            _context2.next = 25;
            break;

          case 22:
            _context2.prev = 22;
            _context2.t0 = _context2['catch'](6);

            // eslint-disable-next-line
            console.log(_context2.t0.message);

          case 25:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined, [[6, 22]]);
  }));

  return function writeGSTR2(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

var writeInvoiceSummary = exports.writeInvoiceSummary = function () {
  var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(startDate, endDate) {
    var orders, stocks, directory, dir;
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _transaction2.default.find({
              createdAt: {
                $gte: startDate,
                $lte: endDate
              },
              type: {
                $eq: 'order'
              },
              subType: {
                $nin: ['discard']
              }
            });

          case 2:
            orders = _context3.sent;
            _context3.next = 5;
            return _transaction2.default.find({
              createdAt: {
                $gte: startDate,
                $lte: endDate
              },
              type: {
                $eq: 'stock'
              }
            });

          case 5:
            stocks = _context3.sent;
            directory = 'Invoice Summary -- ' + new Date(startDate).toDateString() + ' -- ' + new Date(endDate).toDateString();
            _context3.prev = 7;

            if (!_fsExtra2.default.ensureDir(_path2.default.join(REPORTS_ROOT, directory))) {
              _context3.next = 11;
              break;
            }

            _context3.next = 11;
            return _fsExtra2.default.remove(_path2.default.join(REPORTS_ROOT, directory));

          case 11:
            dir = _path2.default.join(_path2.default.join(REPORTS_ROOT, directory, 'Invoice Summary'));
            _context3.next = 14;
            return _fsExtra2.default.mkdirp(dir);

          case 14:

            _fs2.default.writeFileSync(_path2.default.join(dir, 'outgoing.csv'), (0, _reportHelper.generateInvoiceSummary)(orders));
            _fs2.default.writeFileSync(_path2.default.join(dir, 'incoming.csv'), (0, _reportHelper.generateInvoiceSummary)(stocks));
            _context3.next = 21;
            break;

          case 18:
            _context3.prev = 18;
            _context3.t0 = _context3['catch'](7);
            throw _context3.t0;

          case 21:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined, [[7, 18]]);
  }));

  return function writeInvoiceSummary(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();

var writeScheduleDrugReports = exports.writeScheduleDrugReports = function () {
  var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(startDate, endDate) {
    var orders, products, directory, dir;
    return _regenerator2.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return _transaction2.default.find({
              createdAt: {
                $gte: startDate,
                $lte: endDate
              },
              type: {
                $eq: 'order'
              },
              subType: {
                $nin: ['discard']
              }
            });

          case 2:
            orders = _context4.sent;
            _context4.next = 5;
            return _product2.default.find();

          case 5:
            products = _context4.sent;
            directory = 'Schedule Drugs Report -- ' + new Date(startDate).toDateString() + ' -- ' + new Date(endDate).toDateString();
            _context4.prev = 7;

            if (!_fsExtra2.default.ensureDir(_path2.default.join(REPORTS_ROOT, directory))) {
              _context4.next = 11;
              break;
            }

            _context4.next = 11;
            return _fsExtra2.default.remove(_path2.default.join(REPORTS_ROOT, directory));

          case 11:
            dir = _path2.default.join(_path2.default.join(REPORTS_ROOT, directory, 'Schedule Drugs'));
            _context4.next = 14;
            return _fsExtra2.default.mkdirp(dir);

          case 14:

            ['h1', 'h', 'x', 'tb', 'narcotics'].forEach(function (drugType) {
              _fs2.default.writeFileSync(_path2.default.join(dir, drugType + '.csv'), (0, _reportHelper.generateDrugSummary)(orders, products, drugType));
            });
            _context4.next = 20;
            break;

          case 17:
            _context4.prev = 17;
            _context4.t0 = _context4['catch'](7);
            throw _context4.t0;

          case 20:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, undefined, [[7, 17]]);
  }));

  return function writeScheduleDrugReports(_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}();

var writePrescribedDrugReports = exports.writePrescribedDrugReports = function () {
  var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(startDate, endDate) {
    var orders, products, directory, dir;
    return _regenerator2.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return _transaction2.default.find({
              createdAt: {
                $gte: startDate,
                $lte: endDate
              },
              type: {
                $eq: 'order'
              },
              subType: {
                $nin: ['discard']
              }
            });

          case 2:
            orders = _context5.sent;
            _context5.next = 5;
            return _product2.default.find();

          case 5:
            products = _context5.sent;
            directory = 'Prescribed Drugs Report -- ' + new Date(startDate).toDateString() + ' -- ' + new Date(endDate).toDateString();
            _context5.prev = 7;

            if (!_fsExtra2.default.ensureDir(_path2.default.join(REPORTS_ROOT, directory))) {
              _context5.next = 11;
              break;
            }

            _context5.next = 11;
            return _fsExtra2.default.remove(_path2.default.join(REPORTS_ROOT, directory));

          case 11:
            dir = _path2.default.join(_path2.default.join(REPORTS_ROOT, directory, 'Prescribed Drugs'));
            _context5.next = 14;
            return _fsExtra2.default.mkdirp(dir);

          case 14:

            _fs2.default.writeFileSync(_path2.default.join(dir, 'prescribed.csv'), (0, _reportHelper.generatePrescribedDrugsSummary)(orders, products));
            _context5.next = 20;
            break;

          case 17:
            _context5.prev = 17;
            _context5.t0 = _context5['catch'](7);
            throw _context5.t0;

          case 20:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, undefined, [[7, 17]]);
  }));

  return function writePrescribedDrugReports(_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}();

var writeTotalStocks = exports.writeTotalStocks = function () {
  var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6() {
    var currentState, directory, dir;
    return _regenerator2.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return _currentState2.default.findOne().populate([{
              path: 'stocks.products.id',
              select: '_id name basePrice taxes tax discount netPrice costPrice details.prescription'
            }]).populate([{
              path: 'total.products.id',
              select: '_id minQuantity maxQuantity'
            }]);

          case 2:
            currentState = _context6.sent;
            directory = 'Stock Overview';
            _context6.prev = 4;

            if (!_fsExtra2.default.ensureDir(_path2.default.join(REPORTS_ROOT, directory))) {
              _context6.next = 8;
              break;
            }

            _context6.next = 8;
            return _fsExtra2.default.remove(_path2.default.join(REPORTS_ROOT, directory));

          case 8:
            dir = _path2.default.join(_path2.default.join(REPORTS_ROOT, directory, 'Stock Overview'));
            _context6.next = 11;
            return _fsExtra2.default.mkdirp(dir);

          case 11:
            _fs2.default.writeFileSync(_path2.default.join(dir, 'stock.csv'), (0, _reportHelper.generateTotalStock)(currentState.total));
            _context6.next = 17;
            break;

          case 14:
            _context6.prev = 14;
            _context6.t0 = _context6['catch'](4);
            throw _context6.t0;

          case 17:
          case 'end':
            return _context6.stop();
        }
      }
    }, _callee6, undefined, [[4, 14]]);
  }));

  return function writeTotalStocks() {
    return _ref6.apply(this, arguments);
  };
}();