'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateTotalStock = exports.generatePrescribedDrugsSummary = exports.generateDrugSummary = exports.generateIMPGR2 = exports.generateIMPSR2 = exports.generateB2BorB2BURR2 = exports.generateATorATAJR1 = exports.generateEXPR1 = exports.generateCNDURR1 = exports.generateCNDRR1 = exports.generateB2CLR1 = exports.generateB2BR1 = exports.generateInvoiceSummary = exports.generateB2CSR1 = exports.generateHsnSummary = undefined;

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var findObjectWithValue = function findObjectWithValue(array, properties, values) {
  var _loop = function _loop(index) {
    var element = array[index];
    if (properties.map(function (prop, id) {
      return element[prop] === values[id];
    }).reduce(function (a, c) {
      return a && c;
    })) {
      return {
        v: index
      };
    }
  };

  for (var index = 0; index < array.length; index += 1) {
    var _ret = _loop(index);

    if ((typeof _ret === 'undefined' ? 'undefined' : (0, _typeof3.default)(_ret)) === "object") return _ret.v;
  }
  return -1;
};

var ddmmyyyy = function ddmmyyyy(date) {
  var dd = date.getDate();
  var mm = date.getMonth() + 1;
  var yyyy = date.getFullYear();

  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }

  return dd + '-' + mm + '-' + yyyy;
};

var getFinancialYear = function getFinancialYear(dateString) {
  var date = new Date(dateString);
  return date.getFullYear() + '-' + date.getFullYear();
};

var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

var getMonth = function getMonth(dateString) {
  var date = new Date(dateString);
  return months[date.getMonth()];
};

var setValue = function setValue(objectOrArray, keyOrIndex, value) {
  // eslint-disable-next-line
  objectOrArray[keyOrIndex] = value;
};

var applyFunc = function applyFunc(object, funcObject) {
  if (!funcObject) {
    return object;
  }
  var copyObject = _lodash2.default.cloneDeep(object);
  Object.keys(funcObject).forEach(function (key) {
    return setValue(copyObject, key, funcObject[key](copyObject[key]));
  });
  return copyObject;
};

var classify = function classify(arrayofObjects, mergeProps) {
  var restPropsFuncMerge = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var restPropsFunc = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var post = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

  var finalArrayOfObjects = [];

  var _loop2 = function _loop2(index) {
    var element = arrayofObjects[index];
    var aOIndex = findObjectWithValue(finalArrayOfObjects, mergeProps, mergeProps.map(function (prop) {
      return element[prop];
    }));
    if (aOIndex > -1) {
      Object.keys(restPropsFuncMerge).forEach(function (prop) {
        var func = restPropsFuncMerge[prop];
        finalArrayOfObjects[aOIndex][prop] = func(element[prop], finalArrayOfObjects[aOIndex][prop]);
      });
    } else {
      finalArrayOfObjects.push(applyFunc(element, restPropsFunc));
    }
  };

  for (var index = 0; index < arrayofObjects.length; index += 1) {
    _loop2(index);
  }
  return finalArrayOfObjects.map(function (obj) {
    return applyFunc(obj, post);
  });
};

var csvGenerator = function csvGenerator() {
  var headers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var separator = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ',';

  var string = headers.join(separator) + '\n';
  data.forEach(function (line) {
    string = string + '\n' + line.join(separator);
  });
  return string;
};

var classifyHsn = function classifyHsn(orders) {
  var arrayOfProducts = orders.map(function (order) {
    return order.products.filter(function (prod) {
      return prod.hsn && [500, 1200, 1800, 2800].indexOf(prod.tax) > -1;
    }).map(function (prod) {
      var copyProd = _lodash2.default.pick(prod, ['hsn', 'quantity', 'tax']);
      copyProd.total = Number((prod.basePrice * (1 - prod.discount / 10000) * prod.quantity / 100).toFixed(2));
      copyProd.tax = parseInt(prod.tax / 100, 10);
      copyProd.amount = Number((prod.basePrice * (1 - prod.tax / 10000) * (1 - prod.discount / 10000) / 100).toFixed(2));
      return copyProd;
    });
  }).filter(function (arr) {
    return arr.length > 0;
  });
  return classify(_lodash2.default.flatten(arrayOfProducts), ['hsn', 'tax'], {
    amount: function amount(a, c) {
      return a + c;
    },
    total: function total(a, c) {
      return a + c;
    },
    quantity: function quantity(a, c) {
      return a + c;
    }
  }, {
    amount: function amount(a) {
      return a;
    },
    total: function total(a) {
      return a;
    },
    quantity: function quantity(a) {
      return a;
    }
  });
};

var classifyInvoiceTax = function classifyInvoiceTax(orders) {
  var arrayOfProducts = orders.map(function (order) {
    return order.products.filter(function (prod) {
      return [0, 500, 1200, 1800, 2800].indexOf(prod.tax) > -1;
    }).map(function (prod) {
      var copyProd = _lodash2.default.pick(prod, ['num', 'tax']);
      copyProd.customer = order.person && order.person.name || '';
      copyProd.gstin = order.person && order.person.gstin;
      copyProd.num = order.num;
      copyProd.date = order.createdAt;
      copyProd.total = prod.total / 100;
      copyProd.pos = order.person && order.person.address && order.person.address.state || '19-West Bengal'; // hard-coded
      copyProd.tax = parseInt(prod.tax / 100, 10);
      copyProd.taxable = prod.basePrice * prod.quantity / 100;
      return copyProd;
    });
  }).filter(function (arr) {
    return arr.length > 0;
  });
  return classify(_lodash2.default.flatten(arrayOfProducts), ['num', 'tax'], {
    total: function total(a, c) {
      return a + c;
    },
    taxable: function taxable(a, c) {
      return a + c;
    }
  }, {
    total: function total(a) {
      return a;
    },
    taxable: function taxable(a) {
      return a;
    }
  });
};

var classifyInvoiceTaxInRefund = function classifyInvoiceTaxInRefund(orders) {
  var arrayOfProducts = orders.map(function (order) {
    return order.products.filter(function (prod) {
      return [0, 500, 1200, 1800, 2800].indexOf(prod.tax) > -1;
    }).map(function (prod) {
      var copyProd = _lodash2.default.pick(prod, ['num', 'tax']);
      copyProd.customer = order.person && order.person.name || '';
      copyProd.gstin = order.person && order.person.gstin;
      copyProd.num = order.num;
      copyProd.date = order.updatedAt;
      copyProd.total = prod.refund.amount / 100;
      copyProd.pos = order.person && order.person.address && order.person.address.state || '19-West Bengal'; // hard-coded
      copyProd.tax = parseInt(prod.tax / 100, 10);
      copyProd.taxable = prod.refund.amount * prod.quantity / (1 + 1 / prod.tax) / 100;
      return copyProd;
    });
  }).filter(function (arr) {
    return arr.length > 0;
  });
  return classify(_lodash2.default.flatten(arrayOfProducts), ['num', 'tax'], {
    total: function total(a, c) {
      return a + c;
    },
    taxable: function taxable(a, c) {
      return a + c;
    }
  }, {
    total: function total(a) {
      return a;
    },
    taxable: function taxable(a) {
      return a;
    }
  });
};

var getDescription = function getDescription(hsn) {
  if (hsn.substr(0, 2) === '30') {
    return 'Medicine, Drugs and Pharmaceuticals';
  }
  return '';
};

var generateHsnSummary = exports.generateHsnSummary = function generateHsnSummary(orders) {
  var hsnClassified = classifyHsn(orders);
  var headers = ['HSN', 'Description', 'UQC', 'Total Quantity', 'Total Value', 'Taxable Value', 'Integrated Tax Amount', 'Central Tax Amount', 'State/UT Tax Amount', 'Cess Amount'];
  var data = hsnClassified.map(function (prod) {
    return [prod.hsn, getDescription(prod.hsn), 'UNT-UNITS', // hard-coded
    prod.quantity, prod.total.toFixed(2), prod.total.toFixed(2), '', // hard-coded
    (prod.amount / 2).toFixed(2), (prod.amount / 2).toFixed(2), ''];
  } // hard-coded
  );
  return csvGenerator(headers, data);
};

var generateB2CSR1 = exports.generateB2CSR1 = function generateB2CSR1(orders, amended) {
  var headers = [];
  var data = [];
  var taxClassified = classifyInvoiceTax(orders);
  if (amended) {
    headers = ['Financial Year', 'Original Month', 'Place Of Supply', 'Type', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount', 'E-Commerce GSTIN'];
    data = taxClassified.map(function (prod) {
      return [getFinancialYear(prod.date), getMonth(prod.date), '19-West Bengal', // hard-coded
      'OE', // hard-coded
      '', // hard-coded
      prod.tax.toFixed(2), prod.total.toFixed(2), '', // hard-coded
      ''];
    } // hard-coded
    );
  }
  headers = ['Type', 'Place Of Supply', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount', 'E-Commerce GSTIN'];
  data = taxClassified.map(function (prod) {
    return ['OE', // hard-coded
    '19-West Bengal', // hard-coded
    '', // hard-coded
    prod.tax.toFixed(2), prod.total.toFixed(2), '', // hard-coded
    ''];
  } // hard-coded
  );
  return csvGenerator(headers, data);
};

var generateInvoiceSummary = exports.generateInvoiceSummary = function generateInvoiceSummary(orders) {
  var headers = ['Serial', 'Invoice Number', 'Date', 'Customer', 'Gross Amount', 'Discount', 'Taxable Amount', 'Total Tax', 'CGST', 'SGST', 'IGST', 'Net Amount'];
  var data = orders.map(function (order, index) {
    return [index + 1, order.num, ddmmyyyy(new Date(order.createdAt)), order.person && order.person.name || '', (order.price / 100).toFixed(2), (order.discount / 100).toFixed(2), ((order.price - order.discount) / 100).toFixed(2), (order.tax / 100).toFixed(2), order.isInterState ? '' : (order.tax / 200).toFixed(2), order.isInterState ? '' : (order.tax / 200).toFixed(2), order.isInterState ? (order.tax / 100).toFixed(2) : '', (order.total / 100).toFixed(2)];
  });
  return csvGenerator(headers, data);
};

var generateB2BR1 = exports.generateB2BR1 = function generateB2BR1(orders, amended) {
  var classifiedOrders = classifyInvoiceTax(orders);
  classifiedOrders = _lodash2.default.orderBy(classifiedOrders, ['num']);
  var headers = [];
  var data = [];
  if (amended) {
    headers = ['GSTIN/UIN of Recipient', 'Receiver Name', 'Original Invoice Number', 'Original Invoice Date', 'Revised Invoice Number', 'Revised Invoice Date', 'Invoice Value', 'Place Of Supply', 'Reverse Charge', 'Applicable % of Tax Rate', 'Invoice Type', 'E-Commerce GSTIN', 'Rate', 'Taxable Value', 'Cess Amount'];
    data = classifiedOrders.map(function (order) {
      return [order.gstin, order.customer, order.num, ddmmyyyy(new Date(order.date)), '', // amended invoice num
      '', // amended invoice date
      order.total.toFixed(2), order.pos, 'N', // hard-coded
      '', // hard-coded
      'Regular', // hard-coded
      '', // hard-coded
      order.tax.toFixed(2), order.taxable.toFixed(2), ''];
    } // hard-coded
    );
    return csvGenerator(headers, data);
  }
  headers = ['GSTIN/UIN of Recipient', 'Receiver Name', 'Invoice Number', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Reverse Charge', 'Applicable % of Tax Rate', 'Invoice Type', 'E-Commerce GSTIN', 'Rate', 'Taxable Value', 'Cess Amount'];
  data = classifiedOrders.map(function (order) {
    return [order.gstin, order.customer, order.num, ddmmyyyy(new Date(order.date)), order.total.toFixed(2), order.pos, 'N', // hard-coded
    '', // hard-coded
    'Regular', // hard-coded
    '', // hard-coded
    order.tax.toFixed(2), order.taxable.toFixed(2), ''];
  } // hard-coded
  );
  return csvGenerator(headers, data);
};

var generateB2CLR1 = exports.generateB2CLR1 = function generateB2CLR1(orders, amended) {
  var classifiedOrders = classifyInvoiceTax(orders);
  classifiedOrders = _lodash2.default.orderBy(classifiedOrders, ['num']);
  var headers = [];
  var data = [];
  if (amended) {
    headers = ['Original Invoice Number', 'Original Invoice Date', 'Original Place Of Supply', 'Revised Invoice Number', 'Revised Invoice Date', 'Invoice Value', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount', 'E-Commerce GSTIN', 'Sale from Bonded WH'];
    data = classifiedOrders.map(function (order) {
      return [order.num, ddmmyyyy(new Date(order.date)), order.pos, '', // amended invoice num
      '', // amended invoice date
      order.total.toFixed(2), '', // hard-coded
      order.tax.toFixed(2), order.taxable.toFixed(2), '', // hard-coded
      '', // hard-coded
      ''];
    } // hard-coded
    );
    return csvGenerator(headers, data);
  }
  headers = ['Invoice Number', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount', 'E-Commerce GSTIN', 'Sale from Bonded WH'];
  data = classifiedOrders.map(function (order) {
    return [order.num, ddmmyyyy(new Date(order.date)), order.total.toFixed(2), order.pos, '', // hard-coded
    order.tax.toFixed(2), order.taxable.toFixed(2), '', // hard-coded
    '', // hard-coded
    ''];
  } // hard-coded
  );
  return csvGenerator(headers, data);
};

var generateCNDRR1 = exports.generateCNDRR1 = function generateCNDRR1(orders, amended) {
  var classifiedOrders = classifyInvoiceTaxInRefund(orders);
  classifiedOrders = _lodash2.default.orderBy(classifiedOrders, ['num']);
  var headers = [];
  var data = [];
  if (amended) {
    headers = ['GSTIN/UIN of Recipient', 'Receiver Name', 'Original Note/Refund Voucher Number', 'Original Note/Refund Voucher Date', 'Original Invoice/Advance Recipt Number', 'Original Invoice Date', 'Revised Note/Refund Voucher Number', 'Revised Note/Refund Voucher Date', 'Document Type', 'Supply Type', 'Note/Refund Voucher Value', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount', 'Pre GST'];
    data = classifiedOrders.map(function (order) {
      return [order.gstin, order.customer, 'R-' + order.num, ddmmyyyy(new Date(order.date)), order.num, ddmmyyyy(new Date(order.date)), '', '', 'D', // hard-coded
      'Inter State', order.total.toFixed(2), '', // hard-coded
      order.tax.toFixed(2), order.taxable.toFixed(2), '', // hard-coded
      'N'];
    } // hard-coded
    );
    return csvGenerator(headers, data);
  }
  headers = ['GSTIN/UIN of Recipient', 'Receiver Name', 'Invoice/Advance Recipt Number', 'Invoice Date', 'Note/Refund Voucher Number', 'Note/Refund Voucher Date', 'Document Type', 'Place Of Supply', 'Note/Refund Voucher Value', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount', 'Pre GST'];
  data = classifiedOrders.map(function (order) {
    return [order.gstin, order.customer, order.num, ddmmyyyy(new Date(order.date)), 'R-' + order.num, ddmmyyyy(new Date(order.date)), 'D', // hard-coded
    order.pos, order.total.toFixed(2), '', // hard-coded
    order.tax.toFixed(2), order.taxable.toFixed(2), '', // hard-coded
    'N'];
  } // hard-coded
  );
  return csvGenerator(headers, data);
};

var generateCNDURR1 = exports.generateCNDURR1 = function generateCNDURR1(orders, amended) {
  var classifiedOrders = classifyInvoiceTaxInRefund(orders);
  classifiedOrders = _lodash2.default.orderBy(classifiedOrders, ['num']);
  var headers = [];
  var data = [];
  if (amended) {
    headers = ['UR Type', 'Original Note/Refund Voucher Number', 'Original Note/Refund Voucher Date', 'Original Invoice/Advance Recipt Number', 'Original Invoice Date', 'Revised Note/Refund Voucher Number', 'Revised Note/Refund Voucher Date', 'Document Type', 'Supply Type', 'Note/Refund Voucher Value', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount', 'Pre GST'];
    data = classifiedOrders.map(function (order) {
      return ['B2CL', 'R-' + order.num, ddmmyyyy(new Date(order.date)), order.num, ddmmyyyy(new Date(order.date)), '', '', 'D', // hard-coded
      'Inter State', order.total.toFixed(2), '', // hard-coded
      order.tax.toFixed(2), order.taxable.toFixed(2), '', // hard-coded
      'N'];
    } // hard-coded
    );
    return csvGenerator(headers, data);
  }
  headers = ['UR Type', 'Note/Refund Voucher Number', 'Note/Refund Voucher Date', 'Document Type', 'Invoice/Advance Recipt Number', 'Invoice Date', 'Place Of Supply', 'Note/Refund Voucher Value', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount', 'Pre GST'];
  data = classifiedOrders.map(function (order) {
    return ['B2CL', 'R-' + order.num, ddmmyyyy(new Date(order.date)), 'D', // hard-coded
    order.num, ddmmyyyy(new Date(order.date)), order.pos, order.total.toFixed(2), '', // hard-coded
    order.tax.toFixed(2), order.taxable.toFixed(2), '', // hard-coded
    'N'];
  } // hard-coded
  );
  return csvGenerator(headers, data);
};

var generateEXPR1 = exports.generateEXPR1 = function generateEXPR1(amended) {
  var headers = [];
  if (amended) {
    headers = ['Export Type', 'Original Invoice Number', 'Original Invoice Date', 'Revised Invoice Number', 'Revised Invoice Date', 'Invoice Value', 'Port Code', 'Shipping Bill Number', 'Shipping Bill Date', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount'];
    return csvGenerator(headers);
  }
  headers = ['Export Type', 'Invoice Number', 'Invoice Date', 'Invoice Value', 'Port Code', 'Shipping Bill Number', 'Shipping Bill Date', 'Applicable % of Tax Rate', 'Rate', 'Taxable Value', 'Cess Amount'];
  return csvGenerator(headers);
};

var generateATorATAJR1 = exports.generateATorATAJR1 = function generateATorATAJR1(amended) {
  var headers = [];
  if (amended) {
    headers = ['Financial Year', 'Original Month', 'Original Place Of Supply', 'Applicable % of Tax Rate', 'Rate', 'Gross Advance Received', 'Cess Amount'];
    return csvGenerator(headers);
  }
  headers = ['Place Of Supply', 'Applicable % of Tax Rate', 'Rate', 'Gross Advance Received', 'Cess Amount'];
  return csvGenerator(headers);
};

var classifyInvoiceTaxOnStock = function classifyInvoiceTaxOnStock(orders) {
  var arrayOfProducts = orders.map(function (order) {
    return order.products.filter(function (prod) {
      return [0, 500, 1200, 1800, 2800].indexOf(prod.tax) > -1;
    }).map(function (prod) {
      var copyProd = _lodash2.default.pick(prod, ['num', 'tax']);
      copyProd.customer = order.person && order.person.name || '';
      copyProd.gstin = order.person && order.person.gstin;
      copyProd.igst = order.interState;
      copyProd.num = order.report;
      copyProd.invoice = order.invoice;
      copyProd.date = order.createdAt;
      copyProd.total = prod.total / 100;
      copyProd.pos = order.person && order.person.address && order.person.address.state || '19-West Bengal'; // hard-coded
      copyProd.tax = parseInt(prod.tax / 100, 10);
      copyProd.taxable = prod.basePrice * prod.quantity / 100;
      copyProd.amount = prod.basePrice * (prod.quantity * (1 + prod.tax / 10000)) / 100;
      copyProd.igst = order.interState;
      return copyProd;
    });
  }).filter(function (arr) {
    return arr.length > 0;
  });
  return classify(_lodash2.default.flatten(arrayOfProducts), ['num', 'tax'], {
    total: function total(a, c) {
      return a + c;
    },
    taxable: function taxable(a, c) {
      return a + c;
    },
    amount: function amount(a, c) {
      return a + c;
    }
  }, {
    total: function total(a) {
      return a;
    },
    taxable: function taxable(a) {
      return a;
    },
    amount: function amount(a) {
      return a;
    }
  });
};

var generateB2BorB2BURR2 = exports.generateB2BorB2BURR2 = function generateB2BorB2BURR2(orders, registered) {
  var classifiedOrders = classifyInvoiceTaxOnStock(orders);
  classifiedOrders = _lodash2.default.orderBy(classifiedOrders, ['num']);
  var headers = [registered ? 'GSTIN/UIN of Supplier' : 'Supplier Name', 'Invoice Number', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Reverse Charge', 'Invoice Type', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Central Tax Paid', 'State/UT Tax Paid', 'Cess Paid', 'Eligibilty For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Central Tax', 'Availed ITC State/UT Tax', 'Availed ITC Cess'];
  var data = classifiedOrders.map(function (order) {
    return [registered ? order.gstin : order.customer, order.invoice, ddmmyyyy(new Date(order.date)), order.total.toFixed(2), order.pos, 'N', // hard-coded
    'Regular', // hard-coded
    order.tax.toFixed(2), order.taxable.toFixed(2), order.igst ? order.amount.toFixed(2) : '', order.igst ? '' : (order.amount / 2).toFixed(2), order.igst ? '' : (order.amount / 2).toFixed(2), '', // hard-coded
    'Ineligible', // hard-coded
    '', // hard-coded
    '', // hard-coded
    '', // hard-coded
    ''];
  } // hard-coded
  );
  return csvGenerator(headers, data);
};

var generateIMPSR2 = exports.generateIMPSR2 = function generateIMPSR2() {
  var headers = ['Invoice Number of Registered Recipeint', 'Invoice Date', 'Invoice Value', 'Place Of Supply', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Cess Paid', 'Eligibilty For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Cess'];
  return csvGenerator(headers);
};

var generateIMPGR2 = exports.generateIMPGR2 = function generateIMPGR2() {
  var headers = ['Port Code', 'Bill Of Entry Number', 'Bill Of Entry Date', 'Bill Of Entry Value', 'Document type', 'GSTIN Of SEZ Supplier', 'Rate', 'Taxable Value', 'Integrated Tax Paid', 'Cess Paid', 'Eligibility For ITC', 'Availed ITC Integrated Tax', 'Availed ITC Cess'];
  return csvGenerator(headers);
};

var generateDrugSummary = exports.generateDrugSummary = function generateDrugSummary(orders, products) {
  var drugType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'h1';

  var drugs = products.filter(function (p) {
    return p.details && p.details.schedule && p.details.schedule[drugType];
  }).map(function (p) {
    return p._id;
  });
  var drugsMap = {};
  drugs.forEach(function (d) {
    drugsMap[d] = true;
  });
  var data = orders.map(function (order) {
    return order.products.filter(function (p) {
      return drugsMap[p.id];
    }).map(function (p) {
      return [order.num, ddmmyyyy(new Date(order.createdAt)), order.person && order.person.name, p.name, p.quantity, (p.batch || '').replace('_Ref', ''), order.secondPerson && order.secondPerson.name];
    });
  }).reduce(function (a, c) {
    return a.concat(c);
  }, []);
  var headers = ['Invoice', 'Date', 'Customer', 'Drug', 'Quantity', 'Batch', 'Doctor'];
  return csvGenerator(headers, data);
};

var generatePrescribedDrugsSummary = exports.generatePrescribedDrugsSummary = function generatePrescribedDrugsSummary(orders, products) {
  var drugs = products.filter(function (p) {
    return p.details && p.details.prescription;
  }).map(function (p) {
    return p._id;
  });
  var drugsMap = {};
  drugs.forEach(function (d) {
    drugsMap[d] = true;
  });
  var data = orders.map(function (order) {
    return order.products.filter(function (p) {
      return drugsMap[p.id];
    }).map(function (p) {
      return [order.num, ddmmyyyy(new Date(order.createdAt)), order.person && order.person.name, p.name, p.quantity, (p.batch || '').replace('_Ref', ''), order.secondPerson && order.secondPerson.name];
    });
  }).reduce(function (a, c) {
    return a.concat(c);
  }, []);
  var headers = ['Invoice', 'Date', 'Customer', 'Drug', 'Quantity', 'Batch', 'Doctor'];
  return csvGenerator(headers, data);
};

var generateTotalStock = exports.generateTotalStock = function generateTotalStock(stock) {
  var data = stock.products.map(function (p, index) {
    return [index, p.name, p.batch, p.quantity, (p.basePrice / 100).toFixed(2), p.tax.toFixed(2), p.netPrice.toFixed(2), p.expiry ? ddmmyyyy(new Date(p.epxiry)) : ''];
  });
  var headers = ['Index', 'Product Name', 'Batch', 'Quantity', 'Rate', 'Tax', 'Net'];
  return csvGenerator(headers, data);
};