'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.userResolvers = exports.userTypeDefs = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongooseToGraphql = require('mongoose-to-graphql');

var _shelljs = require('shelljs');

var _shelljs2 = _interopRequireDefault(_shelljs);

var _currentState = require('../models/currentState');

var _currentState2 = _interopRequireDefault(_currentState);

var _config = require('../models/config');

var _config2 = _interopRequireDefault(_config);

var _product = require('../models/product');

var _product2 = _interopRequireDefault(_product);

var _transaction = require('../models/transaction');

var _transaction2 = _interopRequireDefault(_transaction);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _helper = require('../helper');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_shelljs2.default.config.execPath = _shelljs2.default.which('hindpos-ausadhi');

var UserGQLSchema = (0, _mongooseToGraphql.modelToType)(_user2.default);
var UserGQLInputSchema = (0, _mongooseToGraphql.modelToCreateType)(_user2.default) + '\n' + (0, _mongooseToGraphql.modelToUpdateType)(_user2.default);

var userTypeDefs = exports.userTypeDefs = {
  output: UserGQLSchema,
  input: UserGQLInputSchema,
  query: ['getUsers (user: UserUpdate, limit: Int, skip: Int): [User]', 'validateUser (user: UserCreate): User', 'shutdown (user: UserCreate, timer: Int): User', 'reset (user: UserCreate): User'],
  mutation: ['createUser(user: UserCreate): User', 'updateUser(oldUser: UserCreate, user: UserCreate): User']
};

var userResolvers = exports.userResolvers = {
  Query: {
    getUsers: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(root, _ref2) {
        var user = _ref2.user,
            limit = _ref2.limit,
            skip = _ref2.skip;
        var dUser, validUsers;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                dUser = (0, _helper.dollarify)(user);
                _context.next = 3;
                return _user2.default.find(dUser).skip(skip || 0).limit(limit || Number.MAX_SAFE_INTEGER);

              case 3:
                validUsers = _context.sent;
                return _context.abrupt('return', validUsers.map(_helper.prepare));

              case 5:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, undefined);
      }));

      return function getUsers(_x, _x2) {
        return _ref.apply(this, arguments);
      };
    }(),
    validateUser: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(root, _ref4) {
        var user = _ref4.user;
        var out;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _user2.default.findOne(user);

              case 3:
                out = _context2.sent;

                if (!(!out || !user.password)) {
                  _context2.next = 6;
                  break;
                }

                throw new Error('Invalid username or password');

              case 6:
                return _context2.abrupt('return', (0, _helper.prepare)(out));

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2['catch'](0);
                throw _context2.t0;

              case 12:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, undefined, [[0, 9]]);
      }));

      return function validateUser(_x3, _x4) {
        return _ref3.apply(this, arguments);
      };
    }(),
    shutdown: function () {
      var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(root, _ref6) {
        var user = _ref6.user,
            timer = _ref6.timer;
        var time;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                time = void 0;

                if (timer) {
                  time = timer * 1000;
                } else {
                  time = 0;
                }
                setTimeout(function () {
                  _shelljs2.default.exec('shutdown -P now');
                }, time);
                return _context3.abrupt('return', user);

              case 4:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, undefined);
      }));

      return function shutdown(_x5, _x6) {
        return _ref5.apply(this, arguments);
      };
    }(),
    reset: function () {
      var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(root, _ref8) {
        var user = _ref8.user;
        var out;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _user2.default.findOne(user);

              case 3:
                out = _context4.sent;

                if (!(!out || !user.password)) {
                  _context4.next = 8;
                  break;
                }

                throw new Error('Incorrect password');

              case 8:
                _currentState2.default.collection.drop();
                _config2.default.collection.drop();
                _product2.default.collection.drop();
                _transaction2.default.collection.drop();
                _user2.default.collection.drop();
                return _context4.abrupt('return', out);

              case 14:
                _context4.next = 19;
                break;

              case 16:
                _context4.prev = 16;
                _context4.t0 = _context4['catch'](0);
                throw _context4.t0;

              case 19:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, undefined, [[0, 16]]);
      }));

      return function reset(_x7, _x8) {
        return _ref7.apply(this, arguments);
      };
    }()
  },
  Mutation: {
    createUser: function () {
      var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(root, args) {
        var user;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                // pass it through a validator
                user = (0, _user2.default)(args.user);
                _context5.prev = 1;
                _context5.t0 = _helper.prepare;
                _context5.next = 5;
                return user.save();

              case 5:
                _context5.t1 = _context5.sent;
                return _context5.abrupt('return', (0, _context5.t0)(_context5.t1));

              case 9:
                _context5.prev = 9;
                _context5.t2 = _context5['catch'](1);
                throw _context5.t2;

              case 12:
              case 'end':
                return _context5.stop();
            }
          }
        }, _callee5, undefined, [[1, 9]]);
      }));

      return function createUser(_x9, _x10) {
        return _ref9.apply(this, arguments);
      };
    }(),
    updateUser: function () {
      var _ref10 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(root, _ref11) {
        var oldUser = _ref11.oldUser,
            user = _ref11.user;
        var isValid, out;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return _user2.default.findOne(oldUser);

              case 3:
                isValid = _context6.sent;

                if (!(!isValid || !user.password)) {
                  _context6.next = 6;
                  break;
                }

                throw new Error('Incorrect password');

              case 6:
                _context6.next = 8;
                return _user2.default.findByIdAndUpdate(isValid._id, user);

              case 8:
                out = _context6.sent;
                _context6.next = 11;
                return _user2.default.findById(isValid._id);

              case 11:
                out = _context6.sent;
                return _context6.abrupt('return', (0, _helper.prepare)(out));

              case 15:
                _context6.prev = 15;
                _context6.t0 = _context6['catch'](0);
                throw _context6.t0;

              case 18:
              case 'end':
                return _context6.stop();
            }
          }
        }, _callee6, undefined, [[0, 15]]);
      }));

      return function updateUser(_x11, _x12) {
        return _ref10.apply(this, arguments);
      };
    }()
  }
};