'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.configResolvers = exports.configTypeDefs = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongooseToGraphql = require('mongoose-to-graphql');

var _config = require('../models/config');

var _config2 = _interopRequireDefault(_config);

var _helper = require('../helper');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ConfigGQLSchema = (0, _mongooseToGraphql.modelToType)(_config2.default);
var ConfigGQLInputSchema = (0, _mongooseToGraphql.modelToCreateType)(_config2.default) + '\n' + (0, _mongooseToGraphql.modelToUpdateType)(_config2.default);

var configTypeDefs = exports.configTypeDefs = {
  output: ConfigGQLSchema,
  input: ConfigGQLInputSchema,
  query: ['getConfig: Config'],
  mutation: ['setConfig(config: ConfigCreate): Config']
};

var configResolvers = exports.configResolvers = {
  Query: {
    getConfig: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var config;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _config2.default.findOne();

              case 2:
                config = _context.sent;
                return _context.abrupt('return', (0, _helper.prepare)(config));

              case 4:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, undefined);
      }));

      return function getConfig() {
        return _ref.apply(this, arguments);
      };
    }()
  },
  Mutation: {
    setConfig: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(root, args) {
        var config, out;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                // pass it through a validator
                config = (0, _config2.default)(args.config);
                _context2.prev = 1;
                _context2.next = 4;
                return _config2.default.findOneAndUpdate({}, config, {
                  upsert: true,
                  setDefaultsOnInsert: true
                });

              case 4:
                out = _context2.sent;
                _context2.next = 7;
                return _config2.default.findOne();

              case 7:
                out = _context2.sent;
                return _context2.abrupt('return', (0, _helper.prepare)(out));

              case 11:
                _context2.prev = 11;
                _context2.t0 = _context2['catch'](1);
                throw _context2.t0;

              case 14:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, undefined, [[1, 11]]);
      }));

      return function setConfig(_x, _x2) {
        return _ref2.apply(this, arguments);
      };
    }()
  }
};