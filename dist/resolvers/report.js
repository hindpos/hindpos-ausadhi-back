'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reportResolvers = exports.reportTypeDefs = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _reportWriter = require('./reportWriter');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var reportTypeDefs = exports.reportTypeDefs = {
  output: 'input REPORT{\n    r1: Boolean\n    r2: Boolean\n    invoice: Boolean\n    schedule: Boolean\n    prescription: Boolean\n    stock: Boolean\n    startDate: String\n    endDate: String\n  }\n  type REPORTRESULT{\n    success: Boolean\n  }\n  ',
  query: ['report (report: REPORT): REPORTRESULT']
};

var reportResolvers = {
  Query: {
    report: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(root, _ref2) {
        var _report = _ref2.report;
        var startDate, endDate;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                startDate = _report.startDate, endDate = _report.endDate;
                _context.prev = 1;

                if (!_report.r1) {
                  _context.next = 5;
                  break;
                }

                _context.next = 5;
                return (0, _reportWriter.writeGSTR1)(startDate, endDate);

              case 5:
                if (!_report.r2) {
                  _context.next = 8;
                  break;
                }

                _context.next = 8;
                return (0, _reportWriter.writeGSTR2)(startDate, endDate);

              case 8:
                if (!_report.invoice) {
                  _context.next = 11;
                  break;
                }

                _context.next = 11;
                return (0, _reportWriter.writeInvoiceSummary)(startDate, endDate);

              case 11:
                if (!_report.schedule) {
                  _context.next = 14;
                  break;
                }

                _context.next = 14;
                return (0, _reportWriter.writeScheduleDrugReports)(startDate, endDate);

              case 14:
                if (!_report.prescription) {
                  _context.next = 17;
                  break;
                }

                _context.next = 17;
                return (0, _reportWriter.writePrescribedDrugReports)(startDate, endDate);

              case 17:
                if (!_report.stock) {
                  _context.next = 20;
                  break;
                }

                _context.next = 20;
                return (0, _reportWriter.writeTotalStocks)();

              case 20:
                _context.next = 26;
                break;

              case 22:
                _context.prev = 22;
                _context.t0 = _context['catch'](1);

                // eslint-disable-next-line
                console.log(_context.t0.message);
                return _context.abrupt('return', { success: false });

              case 26:
                return _context.abrupt('return', { success: true });

              case 27:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, undefined, [[1, 22]]);
      }));

      return function report(_x, _x2) {
        return _ref.apply(this, arguments);
      };
    }()
  }
};
exports.reportResolvers = reportResolvers;