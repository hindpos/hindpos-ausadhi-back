'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.productResolvers = exports.productTypeDefs = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongooseToGraphql = require('mongoose-to-graphql');

var _product = require('../models/product');

var _product2 = _interopRequireDefault(_product);

var _helper = require('../helper');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProductGQLSchema = (0, _mongooseToGraphql.modelToType)(_product2.default);
var ProductGQLInputSchema = (0, _mongooseToGraphql.modelToCreateType)(_product2.default) + '\n' + (0, _mongooseToGraphql.modelToUpdateType)(_product2.default);

var productTypeDefs = exports.productTypeDefs = {
  output: ProductGQLSchema,
  input: ProductGQLInputSchema,
  query: ['getProducts (product: ProductUpdate, limit: Int, skip: Int): [Product]'],
  mutation: ['createProduct(product: ProductCreate): Product', 'updateProduct(filter: ProductUpdate, product: ProductCreate): Product', 'deleteProduct(id: String): Product']
};

var productResolvers = exports.productResolvers = {
  Query: {
    getProducts: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(root, _ref2) {
        var product = _ref2.product,
            limit = _ref2.limit,
            skip = _ref2.skip;
        var dProduct, validProducts;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                dProduct = (0, _helper.dollarify)(product);
                _context.next = 3;
                return _product2.default.find(dProduct).sort({ createdAt: -1 }).skip(skip || 0).limit(limit || Number.MAX_SAFE_INTEGER);

              case 3:
                validProducts = _context.sent;
                return _context.abrupt('return', validProducts.map(_helper.prepare));

              case 5:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, undefined);
      }));

      return function getProducts(_x, _x2) {
        return _ref.apply(this, arguments);
      };
    }()
  },
  Mutation: {
    createProduct: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(root, args) {
        var product;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                // pass it through a validator
                product = (0, _product2.default)(args.product);
                _context2.prev = 1;
                _context2.t0 = _helper.prepare;
                _context2.next = 5;
                return product.save();

              case 5:
                _context2.t1 = _context2.sent;
                return _context2.abrupt('return', (0, _context2.t0)(_context2.t1));

              case 9:
                _context2.prev = 9;
                _context2.t2 = _context2['catch'](1);
                throw _context2.t2;

              case 12:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, undefined, [[1, 9]]);
      }));

      return function createProduct(_x3, _x4) {
        return _ref3.apply(this, arguments);
      };
    }(),
    updateProduct: function () {
      var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(root, _ref5) {
        var filter = _ref5.filter,
            product = _ref5.product;
        var dFilter, out;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                dFilter = (0, _helper.dollarify)(filter);
                _context3.next = 4;
                return _product2.default.findOneAndUpdate(dFilter, product, { upsert: true });

              case 4:
                out = _context3.sent;
                _context3.next = 7;
                return _product2.default.findOne(dFilter);

              case 7:
                out = _context3.sent;
                return _context3.abrupt('return', (0, _helper.prepare)(out));

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3['catch'](0);
                throw _context3.t0;

              case 14:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, undefined, [[0, 11]]);
      }));

      return function updateProduct(_x5, _x6) {
        return _ref4.apply(this, arguments);
      };
    }(),
    deleteProduct: function () {
      var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(root, _ref7) {
        var id = _ref7.id;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _product2.default.find({ _id: id }).remove().exec();

              case 3:
                return _context4.abrupt('return', {
                  _id: '0'
                });

              case 6:
                _context4.prev = 6;
                _context4.t0 = _context4['catch'](0);
                throw _context4.t0;

              case 9:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, undefined, [[0, 6]]);
      }));

      return function deleteProduct(_x7, _x8) {
        return _ref6.apply(this, arguments);
      };
    }()
  }
};