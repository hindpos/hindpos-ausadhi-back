'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.commandResolvers = exports.commandTypeDefs = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongooseToGraphql = require('mongoose-to-graphql');

var _shelljs = require('shelljs');

var _shelljs2 = _interopRequireDefault(_shelljs);

var _currentState = require('../models/currentState');

var _currentState2 = _interopRequireDefault(_currentState);

var _config = require('../models/config');

var _config2 = _interopRequireDefault(_config);

var _product = require('../models/product');

var _product2 = _interopRequireDefault(_product);

var _transaction = require('../models/transaction');

var _transaction2 = _interopRequireDefault(_transaction);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserGQLSchema = (0, _mongooseToGraphql.modelToType)(_user2.default);
var UserGQLInputSchema = (0, _mongooseToGraphql.modelToCreateType)(_user2.default) + '\n' + (0, _mongooseToGraphql.modelToUpdateType)(_user2.default);

_shelljs2.default.config.execPath = _shelljs2.default.which('hindpos-ausadhi');

var commandTypeDefs = exports.commandTypeDefs = {
  input: UserGQLInputSchema,
  output: UserGQLSchema,
  query: ['shutdown (user: UserCreate, timer: Int): User', 'reset (user: UserCreate): User']
};

var commandResolvers = exports.commandResolvers = {
  Query: {
    shutdown: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(root, _ref2) {
        var user = _ref2.user,
            timer = _ref2.timer;
        var time;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                time = void 0;

                if (timer) {
                  time = timer * 1000;
                } else {
                  time = 0;
                }
                setTimeout(function () {
                  _shelljs2.default.exec('shutdown -P now');
                }, time);
                return _context.abrupt('return', user);

              case 4:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, undefined);
      }));

      return function shutdown(_x, _x2) {
        return _ref.apply(this, arguments);
      };
    }(),
    reset: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(root, _ref4) {
        var user = _ref4.user;
        var out;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _user2.default.findOne(user);

              case 3:
                out = _context2.sent;

                if (out) {
                  _context2.next = 8;
                  break;
                }

                throw new Error('Incorrect password');

              case 8:
                _currentState2.default.collection.drop();
                _config2.default.collection.drop();
                _product2.default.collection.drop();
                _transaction2.default.collection.drop();
                _user2.default.collection.drop();
                return _context2.abrupt('return', out);

              case 14:
                _context2.next = 19;
                break;

              case 16:
                _context2.prev = 16;
                _context2.t0 = _context2['catch'](0);
                throw _context2.t0;

              case 19:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, undefined, [[0, 16]]);
      }));

      return function reset(_x3, _x4) {
        return _ref3.apply(this, arguments);
      };
    }()
  }
};