"use strict";

var randArrOf = function randArrOf(generator, number, casual) {
  var iter = casual.integer(0, number);
  var arr = [];
  for (var i = 0; i < iter; i += 1) {
    arr.push(generator());
  }
  return arr;
};

module.exports = { randArrOf: randArrOf };