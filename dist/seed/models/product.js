'use strict';

var _ = require('lodash');

var _require = require('../utils'),
    randArrOf = _require.randArrOf;

var mongoose = require('mongoose');

var extendCasual = function extendCasual(casual) {
  casual.define('myProduct', function () {
    var data = {
      name: casual.name,
      price: casual.myPrice,
      taxes: randArrOf(function () {
        return casual.myTax;
      }, 5, casual),
      discount: casual.myDiscount,
      details: casual.card_data,
      user: new mongoose.mongo.ObjectId(),
      username: casual.username.replace(/[^A-Za-z]/, '')
    };
    data.minQuantity = casual.integer(0, 10);
    data.maxQuantity = casual.integer(1000, 2000);
    return data;
  });
};

module.exports = extendCasual;