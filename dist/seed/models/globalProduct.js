'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _mongodb = require('mongodb');

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _nodeAsyncLoop = require('node-async-loop');

var _nodeAsyncLoop2 = _interopRequireDefault(_nodeAsyncLoop);

var _globalProduct = require('../../models/globalProduct');

var _globalProduct2 = _interopRequireDefault(_globalProduct);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

var url = 'mongodb://localhost:27017';
var dbUrl = 'mongodb://localhost:27017/posapp';

var dbName = 'posapp';
_mongodb.MongoClient.connect(url, function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(err, client) {
    return _regenerator2.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _mongoose2.default.connect(dbUrl, (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
              var db, collection;
              return _regenerator2.default.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      db = client.db(dbName);
                      collection = db.collection('rawProducts');

                      collection.find({}).toArray(function () {
                        var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(err2, docs) {
                          return _regenerator2.default.wrap(function _callee2$(_context2) {
                            while (1) {
                              switch (_context2.prev = _context2.next) {
                                case 0:
                                  (0, _nodeAsyncLoop2.default)(docs, function () {
                                    var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(doc, next) {
                                      var doc2, glob;
                                      return _regenerator2.default.wrap(function _callee$(_context) {
                                        while (1) {
                                          switch (_context.prev = _context.next) {
                                            case 0:
                                              doc2 = doc;

                                              delete doc2._id;
                                              glob = new _globalProduct2.default(doc2);
                                              _context.next = 5;
                                              return glob.save();

                                            case 5:
                                              // console.log(doc);
                                              next();

                                            case 6:
                                            case 'end':
                                              return _context.stop();
                                          }
                                        }
                                      }, _callee, undefined);
                                    }));

                                    return function (_x5, _x6) {
                                      return _ref4.apply(this, arguments);
                                    };
                                  }());

                                case 1:
                                case 'end':
                                  return _context2.stop();
                              }
                            }
                          }, _callee2, undefined);
                        }));

                        return function (_x3, _x4) {
                          return _ref3.apply(this, arguments);
                        };
                      }());

                    case 3:
                    case 'end':
                      return _context3.stop();
                  }
                }
              }, _callee3, undefined);
            })));

          case 1:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, undefined);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());