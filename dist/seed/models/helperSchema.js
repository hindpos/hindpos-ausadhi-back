'use strict';

// import casual from 'casual';
// const casual = require('casual');

/* eslint no-underscore-dangle: ["error", { "allow": ["_phone", "_email"] }] */

var _ = require('lodash');
var mongoose = require('mongoose');

var _require = require('../utils'),
    randArrOf = _require.randArrOf;

var extendCasual = function extendCasual(casual) {
  casual.define('myTax', function () {
    var data = {
      type: casual.random_element(['CGST', 'SGST', 'SAT', 'SET', 'ED', 'VAT', 'CD', 'ET'])
    };
    var percent = casual.integer(0, 100);
    var amount = casual.integer(0, 1000);
    _.extend(data, casual.random_element([{ percent: percent }, { amount: amount }]));
    return data;
  });
  casual.define('myContact', function () {
    return {
      phones: randArrOf(casual._phone, 5, casual),
      emails: randArrOf(casual._email, 5, casual)
    };
  });
  casual.define('myAddress', function () {
    return {
      lines: [casual.address1, casual.address2],
      landmark: casual.street,
      state: casual.state,
      pincode: casual.zip(7),
      country: casual.country
    };
  });
  casual.define('myPerson', function () {
    return {
      name: casual.full_name,
      address: casual.myAddress,
      contacts: casual.myContact,
      company: casual.company_name,
      gstin: '' + casual.card_number,
      license: casual.name
    };
  });
  casual.define('myPayment', function () {
    var data = {
      type: casual.random_element(['cash', 'debit', 'credit', 'cheque', 'dd', 'bank', 'mobile']),
      closed: casual.boolean,
      due: casual.integer(0, 1000),
      history: randArrOf(function () {
        return {
          date: new Date(casual.unix_time),
          amount: casual.integer(1000)
        };
      }, 10, casual)
    };
    if (data.history) {
      data.paid = data.history.reduce(function (acc, nxt) {
        return acc + nxt.amount;
      }, 0);
    }
    if (data.closed) {
      data.due = 0;
    }
    return data;
  });
  casual.define('myPrice', function () {
    var global = casual.random_element([true, false]);
    var data = { global: global };
    if (!global) {
      var customOnGlobal = casual.random_element([{
        percent: casual.integer(-100, 200)
      }, {
        amount: casual.integer(100, 200000)
      }]);
      var custom = casual.integer(0, 20000);
      _.extend(data, casual.random_element([{ custom: custom }, { customOnGlobal: customOnGlobal }]));
    }
    return data;
  });
  casual.define('myDiscount', function () {
    return casual.random_element([{ percent: casual.integer(0, 10000) }, { amount: casual.integer(0, 10000) }]);
  });
  casual.define('myProductTransaction', function () {
    return {
      batch: casual.rgb_hex,
      id: new mongoose.mongo.ObjectId(),
      name: casual.name,
      expiry: new Date(casual.unix_time),
      basePrice: casual.integer(0, 100000),
      discount: casual.myDiscount,
      quantity: casual.integer(0, 100),
      netPrice: casual.integer(0, 100000),
      taxes: randArrOf(function () {
        return casual.myTax;
      }, 5, casual),
      tax: casual.integer(0, 10000),
      total: casual.integer(0, 100000),
      refund: {
        refund: casual.integer(0, 50),
        discard: casual.integer(0, 50),
        keep: casual.integer(0, 50)
      }
    };
  });
};

module.exports = extendCasual;