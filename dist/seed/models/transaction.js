'use strict';

var mongoose = require('mongoose');

var _require = require('../utils'),
    randArrOf = _require.randArrOf;

var extendCasual = function extendCasual(casual) {
  casual.define('myTransaction', function () {
    var data = {
      // num: casual.integer(1, 100000),
      type: casual.random_element(['order', 'stock']),
      attachOrderId: new mongoose.mongo.ObjectId(),
      buyerId: new mongoose.mongo.ObjectId(),
      sellerId: new mongoose.mongo.ObjectId(),
      user: new mongoose.mongo.ObjectId(),
      username: casual.username.replace(/[^A-Za-z]/, ''),
      person: casual.myPerson,
      secondPerson: casual.myPerson,
      products: randArrOf(function () {
        return casual.myProductTransaction;
      }, 10, casual),
      price: casual.integer(100, 100000),
      tax: casual.integer(100, 10000),
      discount: casual.integer(100, 10000),
      total: casual.integer(100, 10000000),
      payment: casual.myPayment
    };
    if (data.type === 'order') {
      data.subType = casual.random_element(['create', 'refund', 'discard', 'profit', 'loss']);
    } else {
      data.subType = casual.random_element(['create', 'refund']);
    }
    return data;
  });
};

module.exports = extendCasual;