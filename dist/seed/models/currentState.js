'use strict';

var _require = require('../utils'),
    randArrOf = _require.randArrOf;

var mongoose = require('mongoose');

var extendCasual = function extendCasual(casual) {
  casual.define('myCurrentStock', function () {
    return {
      stock: new mongoose.mongo.ObjectId(),
      products: randArrOf(function () {
        return casual.myProductTransaction;
      }, 10, casual),
      basePrice: casual.integer(100, 1000000),
      tax: casual.integer(100, 100000),
      netPrice: casual.integer(100, 1000000),
      total: casual.integer(100, 100000)
    };
  });
  casual.define('myCurrentState', function () {
    return {
      stockList: randArrOf(function () {
        return casual.myCurrentStock;
      }, 10, casual),
      total: casual.myCurrentStock
    };
  });
};

module.exports = extendCasual;