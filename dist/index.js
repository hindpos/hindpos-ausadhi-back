#! /usr/bin/env node
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

require('babel-core/register');

require('babel-polyfill');

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _lodash = require('lodash');

var _graphqlTools = require('graphql-tools');

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _graphqlServerExpress = require('graphql-server-express');

var _mongooseToGraphql = require('mongoose-to-graphql');

var _helper = require('./helper');

var _currentState = require('./resolvers/currentState');

var _product = require('./resolvers/product');

var _globalProduct = require('./resolvers/globalProduct');

var _transaction = require('./resolvers/transaction');

var _user = require('./resolvers/user');

var _config = require('./resolvers/config');

var _report = require('./resolvers/report');

var _mongoose = require('./mongoose');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// internal imports

// babel
exports.default = {
  mongooseClose: _mongoose.mongooseClose
};

// file imports


// module imports

var PORT = 3001;
var URL = 'http://localhost';

// Connecting to mongodb
(0, _mongoose.mongooseConnect)();

// Joining all typedefs
var mergedTypeDefs = (0, _helper.addTypeDefs)(_mongooseToGraphql.typeDefs, _currentState.currentStateTypeDefs, _product.productTypeDefs, _globalProduct.globalProductTypeDefs, _transaction.transactionTypeDefs, _user.userTypeDefs, _config.configTypeDefs, _report.reportTypeDefs);

// console.log(typeDefs);

// Merging all resolvers, see merge function from lodash
var mergedResolvers = (0, _lodash.merge)(_mongooseToGraphql.resolvers, _currentState.currentStateResolvers, _product.productResolvers, _globalProduct.globalProductResolvers, _transaction.transactionResolvers, _user.userResolvers, _config.configResolvers, _report.reportResolvers);

// console.dir(resolvers, { depth: 2, colors: true });

var schema = (0, _graphqlTools.makeExecutableSchema)({
  typeDefs: mergedTypeDefs,
  resolvers: mergedResolvers
});

var app = (0, _express2.default)();

app.use((0, _cors2.default)());
app.use('/graphql', _bodyParser2.default.json(), (0, _graphqlServerExpress.graphqlExpress)({ schema: schema }));

var homePath = '/graphiql';

app.use(homePath, (0, _graphqlServerExpress.graphiqlExpress)({
  endpointURL: '/graphql'
}));

app.listen(PORT, function () {
  // eslint-disable-next-line no-console
  console.log('Visit ' + URL + ':' + PORT + homePath);
});