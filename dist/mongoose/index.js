'use strict';

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongod = require('mongod');

var _mongod2 = _interopRequireDefault(_mongod);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mongoDbURI = 'mongodb://localhost:27017';
var database = 'posapp';

_mongoose2.default.Promise = _bluebird2.default;

var server = new _mongod2.default({
  port: 27017,
  dbpath: _path2.default.join(process.env.HOME, '.mongo')
});

var mongooseConnect = function mongooseConnect() {
  server.open(function (err) {
    if (err) {
      console.log(err.message);
    } else {
      _mongoose2.default.connect(mongoDbURI + '/' + database);
    }
  });
};

var mongooseClose = function mongooseClose() {
  _mongoose2.default.connection.close();
  server.close(function (err) {
    console.log(err.message);
  });
};

module.exports = { mongooseConnect: mongooseConnect, mongooseClose: mongooseClose };