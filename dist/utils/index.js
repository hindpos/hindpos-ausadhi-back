"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var checkAll = exports.checkAll = function checkAll(val, fn) {
  if (val !== undefined && val.length > 0) {
    return val.map(fn).reduce(function (curr, nxt) {
      return curr && nxt;
    });
  }
  return true;
};

var isNull = exports.isNull = function isNull(val) {
  if (val) {
    return false;
  }
  return true;
};

var isEmpty = exports.isEmpty = function isEmpty(val) {
  if (val === undefined || val === null) {
    return true;
  }
  if (val === 0) {
    return false;
  }
  if (val instanceof Array && val.length === 0) {
    return true;
  }
  if (val instanceof Object && Object.keys(val).length === 0) {
    return true;
  }
  return false;
};