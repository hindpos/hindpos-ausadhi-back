'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addTypeDefs = exports.dollarify = exports.flattenObject = exports.prepare = undefined;

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _mapKeysDeepLodash = require('map-keys-deep-lodash');

var _mapKeysDeepLodash2 = _interopRequireDefault(_mapKeysDeepLodash);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
var emptyString = '';

var prepare = exports.prepare = function prepare(o) {
  if (!o) {
    return o;
  }
  // eslint-disable-next-line no-param-reassign
  o._id = o._id.toString();
  return o;
};

var flattenObject = exports.flattenObject = function flattenObject(obj) {
  if (!obj) {
    return obj;
  }
  var toReturn = {};
  Object.keys(obj).forEach(function (i) {
    if (Object.keys(obj[i]).length > 0) {
      if ((0, _typeof3.default)(obj[i]) === 'object' && !_lodash2.default.startsWith(Object.keys(obj[i])[0], '$')) {
        var flatObject = flattenObject(obj[i]);
        Object.keys(flatObject).forEach(function (x) {
          toReturn[i + '.' + x] = flatObject[x];
        });
      } else {
        toReturn[i] = obj[i];
      }
    }
  });
  return toReturn;
};

var dollarify = exports.dollarify = function dollarify(obj) {
  if (!obj) {
    return obj;
  }
  var operators = ['lte', 'gte', 'lt', 'gt', 'eq', 'regex', 'options', 'nin'];
  var dollared = (0, _mapKeysDeepLodash2.default)(obj, function (value, key) {
    if (_lodash2.default.includes(operators, key)) {
      return '$' + key;
    }
    return key;
  });
  var flat = flattenObject(dollared);
  // console.log(flat);
  return flat;
};

// Add typedefs, dependencies must go after typedefs on which they depend
var addTypeDefs = exports.addTypeDefs = function addTypeDefs() {
  for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  var genSchema = function genSchema(typeDefArray, property, furtherJoin) {
    return typeDefArray.map(function (typedef) {
      if ((typeof typedef === 'undefined' ? 'undefined' : (0, _typeof3.default)(typedef)) === 'object') {
        if (Object.prototype.hasOwnProperty.call(typedef, property)) {
          if (typedef['' + property]) {
            if (furtherJoin) {
              return typedef['' + property].join('\n');
            }
            return typedef['' + property];
          }
        }
      }
      return emptyString;
    }).join('\n');
  };

  var scalarsSchema = genSchema(args, 'scalar', true);

  var querySchema = 'type Query {\n    ' + genSchema(args, 'query', true) + '\n  }';

  var mutationSchema = 'type Mutation {\n    ' + genSchema(args, 'mutation', true) + '\n  }';

  var inputSchema = genSchema(args, 'input');
  var outputSchema = genSchema(args, 'output');

  var typeDefs = '\n  ' + scalarsSchema + '\n\n  ' + inputSchema + '\n\n  ' + outputSchema + '\n\n  ' + querySchema + '\n\n  ' + mutationSchema + '\n\n  schema {\n      query: Query\n      mutation: Mutation\n  }';

  return typeDefs;
};