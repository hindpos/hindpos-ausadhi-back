'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseTimestamp = require('mongoose-timestamp');

var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);

var _helperSchema = require('./helperSchema');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ConfigSchema = _mongoose2.default.Schema({
  _id: false,
  smallSidebar: {
    $type: Boolean,
    default: false
  },
  autoBill: {
    $type: Boolean,
    default: false
  },
  expiryWarn: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    default: 7,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.']
  },
  printType: {
    $type: String,
    default: 'Thermal'
  },
  target: {
    profit: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      default: 1000,
      validate: [Number.isSafeInteger, 'Number should be a safe integer.']
    },
    revenue: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      default: 10000,
      validate: [Number.isSafeInteger, 'Number should be a safe integer.']
    },
    order: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      default: 100,
      validate: [Number.isSafeInteger, 'Number should be a safe integer.']
    }
  },
  store: {
    id: {
      $type: String,
      required: true
    },
    name: {
      $type: String,
      required: true
    },
    owner: {
      $type: String
    },
    gstin: {
      $type: String
    },
    license: {
      $type: String
    },
    phone: {
      $type: String
    }
  },
  address: _helperSchema.AddressSchema,
  numCopies: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    default: 1,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.']
  }
}, { typeKey: '$type' });

ConfigSchema.plugin(_mongooseTimestamp2.default);
var Config = _mongoose2.default.model('Config', ConfigSchema);

exports.default = Config;