'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProductTransactionSchema = exports.CounterSchema = exports.DiscountSchema = exports.PriceSchema = exports.PaymentSchema = exports.PersonSchema = exports.ContactSchema = exports.AddressSchema = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _utils = require('../utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ContactSchema = _mongoose2.default.Schema({
  _id: false,
  phones: {
    $type: [String]
  },
  emails: {
    $type: [String]
    // validate: [val => checkAll(val, validator.isEmail), 'permissions should be alphanumeric'],
  }
}, { typeKey: '$type' });

var AddressSchema = _mongoose2.default.Schema({
  _id: false,
  lines: {
    $type: [String]
  },
  landmark: {
    $type: String
  },
  state: {
    $type: String
  },
  city: {
    $type: String
  },
  pincode: {
    $type: String
  },
  country: {
    $type: String
  }
}, { typeKey: '$type' });

var PersonSchema = _mongoose2.default.Schema({
  _id: false,
  name: {
    $type: String,
    trim: true
  },
  address: AddressSchema,
  contacts: ContactSchema,
  company: String,
  gstin: String,
  license: String,
  gender: Boolean,
  birthDate: Date,
  age: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer.']
  }
}, { typeKey: '$type' });

var PaymentSchema = _mongoose2.default.Schema({
  _id: false,
  type: {
    $type: String,
    required: true,
    enum: ['cash', 'debit', 'credit', 'cheque', 'dd', 'bank', 'mobile', 'card']
  },
  closed: Boolean,
  paid: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.']
  },
  due: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.']
  },
  history: [{
    _id: false,
    date: Date,
    amount: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      required: true,
      validate: [Number.isSafeInteger, 'Number should be a safe integer.']
    },
    user: _mongoose2.default.Schema.Types.ObjectId,
    username: String
  }],
  details: {
    $type: _mongoose2.default.Schema.Types.Mixed // a json converted to String
  }
}, { typeKey: '$type' });

var PriceSchema = _mongoose2.default.Schema({
  _id: false,
  global: {
    $type: Boolean
  },
  customOnGlobal: {
    percent: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      min: -100
    },
    amount: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      validate: [Number.isSafeInteger, 'Number should be a safe integer']
    }
  },
  custom: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  }
}, { typeKey: '$type' });

PriceSchema.pre('save', function priceValidator(next) {
  if (this.global ^ !(0, _utils.isEmpty)(this.customOnGlobal.percent) ^ !(0, _utils.isEmpty)(this.customOnGlobal.amount) ^ !(0, _utils.isEmpty)(this.custom)) {
    next();
  } else {
    next(new Error('Price can be one of the following types : (global, customOnGlobal, custom)'));
  }
});

var DiscountSchema = _mongoose2.default.Schema({
  _id: false,
  percent: {
    // Percent * 100
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  amount: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  }
}, { typeKey: '$type' });

DiscountSchema.pre('save', function discountValidor(next) {
  if (!(0, _utils.isEmpty)(this.percent) ^ !(0, _utils.isEmpty)(this.amount)) {
    next();
  } else {
    // console.log(this);
    next(new Error('Discount should either be a percentage or an amount'));
  }
});

var CounterSchema = _mongoose2.default.Schema({
  _id: { type: String, required: true },
  order: { type: Number, default: 0 },
  stock: { type: Number, default: 0 },
  credit: { type: Number, default: 0 },
  debit: { type: Number, default: 0 }
});

var ProductTransactionSchema = _mongoose2.default.Schema({
  // _id: false,
  batch: String,
  id: {
    $type: _mongoose2.default.Schema.Types.ObjectId,
    ref: 'Product',
    required: true
  },
  name: String,
  expiry: Date,
  basePrice: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  hsn: String,
  discount: {
    // tax * 100
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  quantity: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  netPrice: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  tax: {
    // tax * 100
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  total: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  costPrice: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  sellPrice: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  refund: {
    refund: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      validate: [Number.isSafeInteger, 'Number should be a safe integer.']
    },
    discard: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      validate: [Number.isSafeInteger, 'Number should be a safe integer.']
    },
    keep: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      validate: [Number.isSafeInteger, 'Number should be a safe integer.']
    },
    amount: {
      $type: Number,
      set: function set(v) {
        return Math.round(v);
      },
      validate: [Number.isSafeInteger, 'Number should be a safe integer.']
    }
  }
}, { typeKey: '$type' });

exports.AddressSchema = AddressSchema;
exports.ContactSchema = ContactSchema;
exports.PersonSchema = PersonSchema;
exports.PaymentSchema = PaymentSchema;
exports.PriceSchema = PriceSchema;
exports.DiscountSchema = DiscountSchema;
exports.CounterSchema = CounterSchema;
exports.ProductTransactionSchema = ProductTransactionSchema;