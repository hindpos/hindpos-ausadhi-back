'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongooseTimestamp = require('mongoose-timestamp');

var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

var _helperSchema = require('./helperSchema');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserSchema = _mongoose2.default.Schema({
  name: {
    $type: String,
    required: true,
    trim: true
  },
  username: {
    $type: String,
    required: true,
    trim: true,
    unique: true,
    validate: [_validator2.default.isAlphanumeric, 'username should be alphanumeric']
  },
  password: String,
  department: {
    $type: String,
    trim: true
  },
  contacts: {
    $type: _helperSchema.ContactSchema
  },
  address: {
    $type: _helperSchema.AddressSchema
  },
  birthDate: Date,
  gender: Boolean,
  admin: {
    $type: Boolean,
    default: false
  },
  permissions: {
    $type: [String]
    // required: true,
  }

}, { typeKey: '$type' });

UserSchema.plugin(_mongooseTimestamp2.default);

var User = _mongoose2.default.model('User', UserSchema);

exports.default = User;