'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseTimestamp = require('mongoose-timestamp');

var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);

var _helperSchema = require('./helperSchema');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CurrentStockSchema = _mongoose2.default.Schema({
  _id: false,
  stock: {
    $type: _mongoose2.default.Schema.Types.ObjectId
    // required: true,
  },
  products: {
    $type: [_helperSchema.ProductTransactionSchema],
    required: true
  },
  price: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    min: 0,
    // required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  tax: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    min: 0,
    // required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  total: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  }
}, { typeKey: '$type' });

var CurrentStateSchema = _mongoose2.default.Schema({
  stocks: {
    $type: [CurrentStockSchema],
    required: true
  },
  total: {
    $type: CurrentStockSchema,
    required: true
  }
}, { typeKey: '$type' });

CurrentStateSchema.plugin(_mongooseTimestamp2.default);

var CurrentState = _mongoose2.default.model('CurrentState', CurrentStateSchema);

exports.default = CurrentState;