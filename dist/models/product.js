'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseTimestamp = require('mongoose-timestamp');

var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProductSchema = _mongoose2.default.Schema({
  name: {
    $type: String,
    required: true,
    trim: true
  },
  basePrice: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  netPrice: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  tax: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  discount: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  minQuantity: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    min: 0
  },
  maxQuantity: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    min: 0
  },
  details: {
    $type: _mongoose2.default.Schema.Types.Mixed
  },
  user: {
    $type: _mongoose2.default.Schema.Types.ObjectId, // buyer in case of stock, and seller in case of order
    required: true
    // Validation not finalised yet!
  },
  username: String
}, { typeKey: '$type' });

ProductSchema.plugin(_mongooseTimestamp2.default);
var Product = _mongoose2.default.model('Product', ProductSchema);

exports.default = Product;