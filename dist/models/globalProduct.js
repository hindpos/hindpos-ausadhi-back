'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseTimestamp = require('mongoose-timestamp');

var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GlobalProductSchema = _mongoose2.default.Schema({
  name: {
    $type: String,
    required: true,
    trim: true
  },
  price: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  unitPrice: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  units: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  tax: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  details: {
    $type: _mongoose2.default.Schema.Types.Mixed // A json coneverted to String
  }
}, { typeKey: '$type' });

GlobalProductSchema.plugin(_mongooseTimestamp2.default);
var GlobalProduct = _mongoose2.default.model('GlobalProduct', GlobalProductSchema);

exports.default = GlobalProduct;