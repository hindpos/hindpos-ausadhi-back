'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseTimestamp = require('mongoose-timestamp');

var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);

var _helperSchema = require('./helperSchema');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TransactionCounter = _mongoose2.default.model('TransactionCounter', _helperSchema.CounterSchema);

var TransactionSchema = _mongoose2.default.Schema({
  num: { // an incremental num
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    // required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer']
  },
  isInterState: {
    $type: Boolean,
    default: false
  },
  type: {
    $type: String,
    required: true,
    enum: ['order', 'stock', 'credit', 'debit']
  },
  subType: {
    $type: String,
    // required: true,
    enum: ['create', 'refund', 'discard', 'profit', 'loss']
  },
  user: {
    $type: _mongoose2.default.Schema.Types.ObjectId, // buyer in case of stock, and seller in case of order
    required: true
  },
  username: String,
  person: _helperSchema.PersonSchema,
  secondPerson: _helperSchema.PersonSchema,
  products: {
    $type: [_helperSchema.ProductTransactionSchema], // A list of productTransaction object which contains
    required: true
  },
  price: { // exclusive of all taxes and discount
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    // We will store the amount of money in paise
    // Comparison on float data$types are bad.
    // required: true,
    validate: {
      validator: Number.isSafeInteger
    }
  },
  tax: { // In paise
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    // required: true,
    min: 0,
    validate: {
      validator: Number.isSafeInteger
    }
  },
  discount: { // In paise
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    // required: true,
    validate: {
      validator: Number.isSafeInteger
    }
  },
  total: { // inclusive of all taxes and discount in paise
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    required: true,
    validate: {
      validator: Number.isSafeInteger
    }
  },
  refundAmount: {
    $type: Number,
    set: function set(v) {
      return Math.round(v);
    },
    validate: {
      validator: Number.isSafeInteger
    }
  },
  refundDate: Date,
  receiptNum: String,
  payment: {
    $type: _helperSchema.PaymentSchema
  },
  description: {
    $type: String
  },
  details: {
    $type: _mongoose2.default.Schema.Types.Mixed // A json coneverted to String
  }
}, { typeKey: '$type' });

TransactionSchema.pre('save', function transactionValidator(next) {
  var _this = this;

  switch (this.type) {
    case 'stock':
      TransactionCounter.findByIdAndUpdate({ _id: 'entityId' }, { $inc: { stock: 1 } }, { new: true, upsert: true }).then(function (count) {
        _this.num = count.stock;
        next();
      });
      break;
    case 'order':
      TransactionCounter.findByIdAndUpdate({ _id: 'entityId' }, { $inc: { order: 1 } }, { new: true, upsert: true }).then(function (count) {
        _this.num = count.order;
        next();
      });
      break;
    case 'debit':
      TransactionCounter.findByIdAndUpdate({ _id: 'entityId' }, { $inc: { debit: 1 } }, { new: true, upsert: true }).then(function (count) {
        _this.num = count.debit;
        next();
      });
      break;
    case 'credit':
      TransactionCounter.findByIdAndUpdate({ _id: 'entityId' }, { $inc: { credit: 1 } }, { new: true, upsert: true }).then(function (count) {
        _this.num = count.credit;
        next();
      });
      break;
    default:
      break;
  }
});

TransactionSchema.plugin(_mongooseTimestamp2.default);
var Transaction = _mongoose2.default.model('Transaction', TransactionSchema);

exports.default = Transaction;