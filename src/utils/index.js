export const checkAll = (val, fn) => {
  if (val !== undefined && val.length > 0) {
    return val.map(fn).reduce((curr, nxt) => curr && nxt);
  }
  return true;
};

export const isNull = (val) => {
  if (val) {
    return false;
  }
  return true;
};

export const isEmpty = (val) => {
  if (val === undefined || val === null) {
    return true;
  }
  if (val === 0) {
    return false;
  }
  if (val instanceof Array && (val.length === 0)) {
    return true;
  }
  if (val instanceof Object && (Object.keys(val).length === 0)) {
    return true;
  }
  return false;
};
