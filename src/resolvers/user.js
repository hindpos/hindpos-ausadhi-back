import { modelToType, modelToCreateType, modelToUpdateType } from 'mongoose-to-graphql';
import shell from 'shelljs';
import CurrentState from '../models/currentState';
import Config from '../models/config';
import Product from '../models/product';
import Transaction from '../models/transaction';
import User from '../models/user';

import { prepare, dollarify } from '../helper';

shell.config.execPath = shell.which('hindpos-ausadhi');

const UserGQLSchema = modelToType(User);
const UserGQLInputSchema = `${modelToCreateType(User)}\n${modelToUpdateType(User)}`;

export const userTypeDefs = {
  output: UserGQLSchema,
  input: UserGQLInputSchema,
  query: [
    'getUsers (user: UserUpdate, limit: Int, skip: Int): [User]',
    'validateUser (user: UserCreate): User',
    'shutdown (user: UserCreate, timer: Int): User',
    'reset (user: UserCreate): User',
  ],
  mutation: [
    'createUser(user: UserCreate): User',
    'updateUser(oldUser: UserCreate, user: UserCreate): User',
  ],
};

export const userResolvers = {
  Query: {
    getUsers: async (root, { user, limit, skip }) => {
      const dUser = dollarify(user);
      const validUsers = await User.find(dUser)
        .skip(skip || 0)
        .limit(limit || Number.MAX_SAFE_INTEGER);
      return validUsers.map(prepare);
    },
    validateUser: async (root, { user }) => {
      try {
        const out = await User.findOne(user);
        if (!out || !(user.password)) {
          throw new Error('Invalid username or password');
        }
        return prepare(out);
      } catch (err) {
        throw err;
      }
    },
    shutdown: async (root, { user, timer }) => {
      let time;
      if (timer) {
        time = timer * 1000;
      } else {
        time = 0;
      }
      setTimeout(() => {
        shell.exec('shutdown -P now');
      }, time);
      return user;
    },
    reset: async (root, { user }) => {
      try {
        const out = await User.findOne(user);
        if (!out || !(user.password)) {
          throw new Error('Incorrect password');
        } else {
          CurrentState.collection.drop();
          Config.collection.drop();
          Product.collection.drop();
          Transaction.collection.drop();
          User.collection.drop();
          return out;
        }
      } catch (err) {
        throw err;
      }
    },
  },
  Mutation: {
    createUser: async (root, args) => {
      // pass it through a validator
      const user = User(args.user);
      try {
        return prepare(await user.save());
      } catch (err) {
        throw err;
      }
    },
    updateUser: async (root, { oldUser, user }) => {
      try {
        const isValid = await User.findOne(oldUser);
        if (!isValid || !(user.password)) {
          throw new Error('Incorrect password');
        }
        let out = await User.findByIdAndUpdate(isValid._id, user);
        out = await User.findById(isValid._id);
        return prepare(out);
      } catch (err) {
        throw err;
      }
    },
  },
};
