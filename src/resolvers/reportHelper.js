import _ from 'lodash';

const findObjectWithValue = (array, properties, values) => {
  for (let index = 0; index < array.length; index += 1) {
    const element = array[index];
    if (properties.map((prop, id) => element[prop] === values[id])
      .reduce((a, c) => a && c)) {
      return index;
    }
  }
  return -1;
};

const ddmmyyyy = (date) => {
  let dd = date.getDate();
  let mm = date.getMonth() + 1;
  const yyyy = date.getFullYear();

  if (dd < 10) {
    dd = `0${dd}`;
  }
  if (mm < 10) {
    mm = `0${mm}`;
  }

  return `${dd}-${mm}-${yyyy}`;
};

const getFinancialYear = (dateString) => {
  const date = new Date(dateString);
  return `${date.getFullYear()}-${date.getFullYear()}`;
};

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const getMonth = (dateString) => {
  const date = new Date(dateString);
  return months[date.getMonth()];
};

const setValue = (objectOrArray, keyOrIndex, value) => {
  // eslint-disable-next-line
  objectOrArray[keyOrIndex] = value;
};

const applyFunc = (object, funcObject) => {
  if (!funcObject) {
    return object;
  }
  const copyObject = _.cloneDeep(object);
  Object.keys(funcObject).forEach(key => setValue(
    copyObject,
    key,
    funcObject[key](copyObject[key]),
  ));
  return copyObject;
};

const classify = (
  arrayofObjects,
  mergeProps,
  restPropsFuncMerge = {},
  restPropsFunc = {},
  post = {},
) => {
  const finalArrayOfObjects = [];
  for (let index = 0; index < arrayofObjects.length; index += 1) {
    const element = arrayofObjects[index];
    const aOIndex = findObjectWithValue(
      finalArrayOfObjects,
      mergeProps,
      mergeProps.map(prop => element[prop]),
    );
    if (aOIndex > -1) {
      Object.keys(restPropsFuncMerge).forEach((prop) => {
        const func = restPropsFuncMerge[prop];
        finalArrayOfObjects[aOIndex][prop] = func(
          element[prop],
          finalArrayOfObjects[aOIndex][prop],
        );
      });
    } else {
      finalArrayOfObjects.push(applyFunc(element, restPropsFunc));
    }
  }
  return finalArrayOfObjects.map(obj => applyFunc(obj, post));
};

const csvGenerator = (headers = [], data = [], separator = ',') => {
  let string = `${headers.join(separator)}\n`;
  data.forEach((line) => {
    string = `${string}\n${line.join(separator)}`;
  });
  return string;
};

const classifyHsn = (orders) => {
  const arrayOfProducts = orders.map(order => order.products
    .filter(prod => prod.hsn && [500, 1200, 1800, 2800].indexOf(prod.tax) > -1)
    .map((prod) => {
      const copyProd = _.pick(prod, ['hsn', 'quantity', 'tax']);
      copyProd.total = Number(((prod.basePrice
        * (1 - (prod.discount / 10000)) * prod.quantity) / 100).toFixed(2));
      copyProd.tax = parseInt(prod.tax / 100, 10);
      copyProd.amount = Number(((prod.basePrice
        * (1 - (prod.tax / 10000))
        * (1 - (prod.discount / 10000))) / 100).toFixed(2));
      return copyProd;
    }))
    .filter(arr => arr.length > 0);
  return classify(_.flatten(arrayOfProducts), ['hsn', 'tax'], {
    amount: (a, c) => a + c,
    total: (a, c) => a + c,
    quantity: (a, c) => a + c,
  }, {
    amount: a => a,
    total: a => a,
    quantity: a => a,
  });
};

const classifyInvoiceTax = (orders) => {
  const arrayOfProducts = orders.map(order => order.products
    .filter(prod => [0, 500, 1200, 1800, 2800].indexOf(prod.tax) > -1)
    .map((prod) => {
      const copyProd = _.pick(prod, ['num', 'tax']);
      copyProd.customer = ((order.person && order.person.name) || '');
      copyProd.gstin = (order.person && order.person.gstin);
      copyProd.num = order.num;
      copyProd.date = order.createdAt;
      copyProd.total = (prod.total / 100);
      copyProd.pos = (order.person && order.person.address && order.person.address.state) || '19-West Bengal'; // hard-coded
      copyProd.tax = parseInt(prod.tax / 100, 10);
      copyProd.taxable = ((prod.basePrice * prod.quantity) / 100);
      return copyProd;
    }))
    .filter(arr => arr.length > 0);
  return classify(_.flatten(arrayOfProducts), ['num', 'tax'], {
    total: (a, c) => a + c,
    taxable: (a, c) => a + c,
  }, {
    total: a => a,
    taxable: a => a,
  });
};

const classifyInvoiceTaxInRefund = (orders) => {
  const arrayOfProducts = orders.map(order => order.products
    .filter(prod => [0, 500, 1200, 1800, 2800].indexOf(prod.tax) > -1)
    .map((prod) => {
      const copyProd = _.pick(prod, ['num', 'tax']);
      copyProd.customer = ((order.person && order.person.name) || '');
      copyProd.gstin = (order.person && order.person.gstin);
      copyProd.num = order.num;
      copyProd.date = order.updatedAt;
      copyProd.total = (prod.refund.amount / 100);
      copyProd.pos = (order.person && order.person.address && order.person.address.state) || '19-West Bengal'; // hard-coded
      copyProd.tax = parseInt(prod.tax / 100, 10);
      copyProd.taxable = (((prod.refund.amount * prod.quantity) / (1 + (1 / prod.tax))) / 100);
      return copyProd;
    }))
    .filter(arr => arr.length > 0);
  return classify(_.flatten(arrayOfProducts), ['num', 'tax'], {
    total: (a, c) => a + c,
    taxable: (a, c) => a + c,
  }, {
    total: a => a,
    taxable: a => a,
  });
};

const getDescription = (hsn) => {
  if (hsn.substr(0, 2) === '30') {
    return 'Medicine, Drugs and Pharmaceuticals';
  }
  return '';
};

export const generateHsnSummary = (orders) => {
  const hsnClassified = classifyHsn(orders);
  const headers = [
    'HSN',
    'Description',
    'UQC',
    'Total Quantity',
    'Total Value',
    'Taxable Value',
    'Integrated Tax Amount',
    'Central Tax Amount',
    'State/UT Tax Amount',
    'Cess Amount',
  ];
  const data = hsnClassified.map(prod => [
    prod.hsn,
    getDescription(prod.hsn),
    'UNT-UNITS', // hard-coded
    prod.quantity,
    prod.total.toFixed(2),
    prod.total.toFixed(2),
    '', // hard-coded
    (prod.amount / 2).toFixed(2),
    (prod.amount / 2).toFixed(2),
    '', // hard-coded
  ]);
  return csvGenerator(headers, data);
};


export const generateB2CSR1 = (orders, amended) => {
  let headers = [];
  let data = [];
  const taxClassified = classifyInvoiceTax(orders);
  if (amended) {
    headers = [
      'Financial Year',
      'Original Month',
      'Place Of Supply',
      'Type',
      'Applicable % of Tax Rate',
      'Rate',
      'Taxable Value',
      'Cess Amount',
      'E-Commerce GSTIN',
    ];
    data = taxClassified.map(prod => [
      getFinancialYear(prod.date),
      getMonth(prod.date),
      '19-West Bengal', // hard-coded
      'OE', // hard-coded
      '', // hard-coded
      prod.tax.toFixed(2),
      prod.total.toFixed(2),
      '', // hard-coded
      '', // hard-coded
    ]);
  }
  headers = [
    'Type',
    'Place Of Supply',
    'Applicable % of Tax Rate',
    'Rate',
    'Taxable Value',
    'Cess Amount',
    'E-Commerce GSTIN',
  ];
  data = taxClassified.map(prod => [
    'OE', // hard-coded
    '19-West Bengal', // hard-coded
    '', // hard-coded
    prod.tax.toFixed(2),
    prod.total.toFixed(2),
    '', // hard-coded
    '', // hard-coded
  ]);
  return csvGenerator(headers, data);
};

export const generateInvoiceSummary = (orders) => {
  const headers = [
    'Serial',
    'Invoice Number',
    'Date',
    'Customer',
    'Gross Amount',
    'Discount',
    'Taxable Amount',
    'Total Tax',
    'CGST',
    'SGST',
    'IGST',
    'Net Amount',
  ];
  const data = orders.map((order, index) => [
    (index + 1),
    order.num,
    ddmmyyyy(new Date(order.createdAt)),
    (order.person && order.person.name) || '',
    (order.price / 100).toFixed(2),
    (order.discount / 100).toFixed(2),
    ((order.price - order.discount) / 100).toFixed(2),
    (order.tax / 100).toFixed(2),
    (order.isInterState) ? '' : (order.tax / 200).toFixed(2),
    (order.isInterState) ? '' : (order.tax / 200).toFixed(2),
    (order.isInterState) ? (order.tax / 100).toFixed(2) : '',
    (order.total / 100).toFixed(2),
  ]);
  return csvGenerator(headers, data);
};

export const generateB2BR1 = (orders, amended) => {
  let classifiedOrders = classifyInvoiceTax(orders);
  classifiedOrders = _.orderBy(classifiedOrders, ['num']);
  let headers = [];
  let data = [];
  if (amended) {
    headers = [
      'GSTIN/UIN of Recipient',
      'Receiver Name',
      'Original Invoice Number',
      'Original Invoice Date',
      'Revised Invoice Number',
      'Revised Invoice Date',
      'Invoice Value',
      'Place Of Supply',
      'Reverse Charge',
      'Applicable % of Tax Rate',
      'Invoice Type',
      'E-Commerce GSTIN',
      'Rate',
      'Taxable Value',
      'Cess Amount',
    ];
    data = classifiedOrders.map(order => [
      order.gstin,
      order.customer,
      order.num,
      ddmmyyyy(new Date(order.date)),
      '', // amended invoice num
      '', // amended invoice date
      order.total.toFixed(2),
      order.pos,
      'N', // hard-coded
      '', // hard-coded
      'Regular', // hard-coded
      '', // hard-coded
      order.tax.toFixed(2),
      order.taxable.toFixed(2),
      '', // hard-coded
    ]);
    return csvGenerator(headers, data);
  }
  headers = [
    'GSTIN/UIN of Recipient',
    'Receiver Name',
    'Invoice Number',
    'Invoice Date',
    'Invoice Value',
    'Place Of Supply',
    'Reverse Charge',
    'Applicable % of Tax Rate',
    'Invoice Type',
    'E-Commerce GSTIN',
    'Rate',
    'Taxable Value',
    'Cess Amount',
  ];
  data = classifiedOrders.map(order => [
    order.gstin,
    order.customer,
    order.num,
    ddmmyyyy(new Date(order.date)),
    order.total.toFixed(2),
    order.pos,
    'N', // hard-coded
    '', // hard-coded
    'Regular', // hard-coded
    '', // hard-coded
    order.tax.toFixed(2),
    order.taxable.toFixed(2),
    '', // hard-coded
  ]);
  return csvGenerator(headers, data);
};

export const generateB2CLR1 = (orders, amended) => {
  let classifiedOrders = classifyInvoiceTax(orders);
  classifiedOrders = _.orderBy(classifiedOrders, ['num']);
  let headers = [];
  let data = [];
  if (amended) {
    headers = [
      'Original Invoice Number',
      'Original Invoice Date',
      'Original Place Of Supply',
      'Revised Invoice Number',
      'Revised Invoice Date',
      'Invoice Value',
      'Applicable % of Tax Rate',
      'Rate',
      'Taxable Value',
      'Cess Amount',
      'E-Commerce GSTIN',
      'Sale from Bonded WH',
    ];
    data = classifiedOrders.map(order => [
      order.num,
      ddmmyyyy(new Date(order.date)),
      order.pos,
      '', // amended invoice num
      '', // amended invoice date
      order.total.toFixed(2),
      '', // hard-coded
      order.tax.toFixed(2),
      order.taxable.toFixed(2),
      '', // hard-coded
      '', // hard-coded
      '', // hard-coded
    ]);
    return csvGenerator(headers, data);
  }
  headers = [
    'Invoice Number',
    'Invoice Date',
    'Invoice Value',
    'Place Of Supply',
    'Applicable % of Tax Rate',
    'Rate',
    'Taxable Value',
    'Cess Amount',
    'E-Commerce GSTIN',
    'Sale from Bonded WH',
  ];
  data = classifiedOrders.map(order => [
    order.num,
    ddmmyyyy(new Date(order.date)),
    order.total.toFixed(2),
    order.pos,
    '', // hard-coded
    order.tax.toFixed(2),
    order.taxable.toFixed(2),
    '', // hard-coded
    '', // hard-coded
    '', // hard-coded
  ]);
  return csvGenerator(headers, data);
};

export const generateCNDRR1 = (orders, amended) => {
  let classifiedOrders = classifyInvoiceTaxInRefund(orders);
  classifiedOrders = _.orderBy(classifiedOrders, ['num']);
  let headers = [];
  let data = [];
  if (amended) {
    headers = [
      'GSTIN/UIN of Recipient',
      'Receiver Name',
      'Original Note/Refund Voucher Number',
      'Original Note/Refund Voucher Date',
      'Original Invoice/Advance Recipt Number',
      'Original Invoice Date',
      'Revised Note/Refund Voucher Number',
      'Revised Note/Refund Voucher Date',
      'Document Type',
      'Supply Type',
      'Note/Refund Voucher Value',
      'Applicable % of Tax Rate',
      'Rate',
      'Taxable Value',
      'Cess Amount',
      'Pre GST',
    ];
    data = classifiedOrders.map(order => [
      order.gstin,
      order.customer,
      `R-${order.num}`,
      ddmmyyyy(new Date(order.date)),
      order.num,
      ddmmyyyy(new Date(order.date)),
      '',
      '',
      'D', // hard-coded
      'Inter State',
      order.total.toFixed(2),
      '', // hard-coded
      order.tax.toFixed(2),
      order.taxable.toFixed(2),
      '', // hard-coded
      'N', // hard-coded
    ]);
    return csvGenerator(headers, data);
  }
  headers = [
    'GSTIN/UIN of Recipient',
    'Receiver Name',
    'Invoice/Advance Recipt Number',
    'Invoice Date',
    'Note/Refund Voucher Number',
    'Note/Refund Voucher Date',
    'Document Type',
    'Place Of Supply',
    'Note/Refund Voucher Value',
    'Applicable % of Tax Rate',
    'Rate',
    'Taxable Value',
    'Cess Amount',
    'Pre GST',
  ];
  data = classifiedOrders.map(order => [
    order.gstin,
    order.customer,
    order.num,
    ddmmyyyy(new Date(order.date)),
    `R-${order.num}`,
    ddmmyyyy(new Date(order.date)),
    'D', // hard-coded
    order.pos,
    order.total.toFixed(2),
    '', // hard-coded
    order.tax.toFixed(2),
    order.taxable.toFixed(2),
    '', // hard-coded
    'N', // hard-coded
  ]);
  return csvGenerator(headers, data);
};

export const generateCNDURR1 = (orders, amended) => {
  let classifiedOrders = classifyInvoiceTaxInRefund(orders);
  classifiedOrders = _.orderBy(classifiedOrders, ['num']);
  let headers = [];
  let data = [];
  if (amended) {
    headers = [
      'UR Type',
      'Original Note/Refund Voucher Number',
      'Original Note/Refund Voucher Date',
      'Original Invoice/Advance Recipt Number',
      'Original Invoice Date',
      'Revised Note/Refund Voucher Number',
      'Revised Note/Refund Voucher Date',
      'Document Type',
      'Supply Type',
      'Note/Refund Voucher Value',
      'Applicable % of Tax Rate',
      'Rate',
      'Taxable Value',
      'Cess Amount',
      'Pre GST',
    ];
    data = classifiedOrders.map(order => [
      'B2CL',
      `R-${order.num}`,
      ddmmyyyy(new Date(order.date)),
      order.num,
      ddmmyyyy(new Date(order.date)),
      '',
      '',
      'D', // hard-coded
      'Inter State',
      order.total.toFixed(2),
      '', // hard-coded
      order.tax.toFixed(2),
      order.taxable.toFixed(2),
      '', // hard-coded
      'N', // hard-coded
    ]);
    return csvGenerator(headers, data);
  }
  headers = [
    'UR Type',
    'Note/Refund Voucher Number',
    'Note/Refund Voucher Date',
    'Document Type',
    'Invoice/Advance Recipt Number',
    'Invoice Date',
    'Place Of Supply',
    'Note/Refund Voucher Value',
    'Applicable % of Tax Rate',
    'Rate',
    'Taxable Value',
    'Cess Amount',
    'Pre GST',
  ];
  data = classifiedOrders.map(order => [
    'B2CL',
    `R-${order.num}`,
    ddmmyyyy(new Date(order.date)),
    'D', // hard-coded
    order.num,
    ddmmyyyy(new Date(order.date)),
    order.pos,
    order.total.toFixed(2),
    '', // hard-coded
    order.tax.toFixed(2),
    order.taxable.toFixed(2),
    '', // hard-coded
    'N', // hard-coded
  ]);
  return csvGenerator(headers, data);
};

export const generateEXPR1 = (amended) => {
  let headers = [];
  if (amended) {
    headers = [
      'Export Type',
      'Original Invoice Number',
      'Original Invoice Date',
      'Revised Invoice Number',
      'Revised Invoice Date',
      'Invoice Value',
      'Port Code',
      'Shipping Bill Number',
      'Shipping Bill Date',
      'Applicable % of Tax Rate',
      'Rate',
      'Taxable Value',
      'Cess Amount',
    ];
    return csvGenerator(headers);
  }
  headers = [
    'Export Type',
    'Invoice Number',
    'Invoice Date',
    'Invoice Value',
    'Port Code',
    'Shipping Bill Number',
    'Shipping Bill Date',
    'Applicable % of Tax Rate',
    'Rate',
    'Taxable Value',
    'Cess Amount',
  ];
  return csvGenerator(headers);
};

export const generateATorATAJR1 = (amended) => {
  let headers = [];
  if (amended) {
    headers = [
      'Financial Year',
      'Original Month',
      'Original Place Of Supply',
      'Applicable % of Tax Rate',
      'Rate',
      'Gross Advance Received',
      'Cess Amount',
    ];
    return csvGenerator(headers);
  }
  headers = [
    'Place Of Supply',
    'Applicable % of Tax Rate',
    'Rate',
    'Gross Advance Received',
    'Cess Amount',
  ];
  return csvGenerator(headers);
};

const classifyInvoiceTaxOnStock = (orders) => {
  const arrayOfProducts = orders.map(order => order.products
    .filter(prod => [0, 500, 1200, 1800, 2800].indexOf(prod.tax) > -1)
    .map((prod) => {
      const copyProd = _.pick(prod, ['num', 'tax']);
      copyProd.customer = ((order.person && order.person.name) || '');
      copyProd.gstin = (order.person && order.person.gstin);
      copyProd.igst = order.interState;
      copyProd.num = order.report;
      copyProd.invoice = order.invoice;
      copyProd.date = order.createdAt;
      copyProd.total = (prod.total / 100);
      copyProd.pos = (order.person && order.person.address && order.person.address.state) || '19-West Bengal'; // hard-coded
      copyProd.tax = parseInt(prod.tax / 100, 10);
      copyProd.taxable = ((prod.basePrice * prod.quantity) / 100);
      copyProd.amount = ((prod.basePrice
        * (prod.quantity * (1 + (prod.tax / 10000)))) / 100);
      copyProd.igst = order.interState;
      return copyProd;
    }))
    .filter(arr => arr.length > 0);
  return classify(_.flatten(arrayOfProducts), ['num', 'tax'], {
    total: (a, c) => a + c,
    taxable: (a, c) => a + c,
    amount: (a, c) => a + c,
  }, {
    total: a => a,
    taxable: a => a,
    amount: a => a,
  });
};

export const generateB2BorB2BURR2 = (orders, registered) => {
  let classifiedOrders = classifyInvoiceTaxOnStock(orders);
  classifiedOrders = _.orderBy(classifiedOrders, ['num']);
  const headers = [
    (registered ? 'GSTIN/UIN of Supplier' : 'Supplier Name'),
    'Invoice Number',
    'Invoice Date',
    'Invoice Value',
    'Place Of Supply',
    'Reverse Charge',
    'Invoice Type',
    'Rate',
    'Taxable Value',
    'Integrated Tax Paid',
    'Central Tax Paid',
    'State/UT Tax Paid',
    'Cess Paid',
    'Eligibilty For ITC',
    'Availed ITC Integrated Tax',
    'Availed ITC Central Tax',
    'Availed ITC State/UT Tax',
    'Availed ITC Cess',
  ];
  const data = classifiedOrders.map(order => [
    (registered ? order.gstin : order.customer),
    order.invoice,
    ddmmyyyy(new Date(order.date)),
    order.total.toFixed(2),
    order.pos,
    'N', // hard-coded
    'Regular', // hard-coded
    order.tax.toFixed(2),
    order.taxable.toFixed(2),
    (order.igst ? order.amount.toFixed(2) : ''),
    (order.igst ? '' : (order.amount / 2).toFixed(2)),
    (order.igst ? '' : (order.amount / 2).toFixed(2)),
    '', // hard-coded
    'Ineligible', // hard-coded
    '', // hard-coded
    '', // hard-coded
    '', // hard-coded
    '', // hard-coded
  ]);
  return csvGenerator(headers, data);
};

export const generateIMPSR2 = () => {
  const headers = [
    'Invoice Number of Registered Recipeint',
    'Invoice Date',
    'Invoice Value',
    'Place Of Supply',
    'Rate',
    'Taxable Value',
    'Integrated Tax Paid',
    'Cess Paid',
    'Eligibilty For ITC',
    'Availed ITC Integrated Tax',
    'Availed ITC Cess',
  ];
  return csvGenerator(headers);
};

export const generateIMPGR2 = () => {
  const headers = [
    'Port Code',
    'Bill Of Entry Number',
    'Bill Of Entry Date',
    'Bill Of Entry Value',
    'Document type',
    'GSTIN Of SEZ Supplier',
    'Rate',
    'Taxable Value',
    'Integrated Tax Paid',
    'Cess Paid',
    'Eligibility For ITC',
    'Availed ITC Integrated Tax',
    'Availed ITC Cess',
  ];
  return csvGenerator(headers);
};

export const generateDrugSummary = (orders, products, drugType = 'h1') => {
  const drugs = products
    .filter(p => p.details && p.details.schedule && p.details.schedule[drugType])
    .map(p => p._id);
  const drugsMap = {};
  drugs.forEach((d) => {
    drugsMap[d] = true;
  });
  const data = orders.map(order => order.products
    .filter(p => drugsMap[p.id])
    .map(p => [
      order.num,
      ddmmyyyy(new Date(order.createdAt)),
      order.person && order.person.name,
      p.name,
      p.quantity,
      (p.batch || '').replace('_Ref', ''),
      order.secondPerson && order.secondPerson.name,
    ]))
    .reduce((a, c) => a.concat(c), []);
  const headers = [
    'Invoice',
    'Date',
    'Customer',
    'Drug',
    'Quantity',
    'Batch',
    'Doctor',
  ];
  return csvGenerator(headers, data);
};

export const generatePrescribedDrugsSummary = (orders, products) => {
  const drugs = products
    .filter(p => p.details && p.details.prescription)
    .map(p => p._id);
  const drugsMap = {};
  drugs.forEach((d) => {
    drugsMap[d] = true;
  });
  const data = orders.map(order => order.products
    .filter(p => drugsMap[p.id])
    .map(p => [
      order.num,
      ddmmyyyy(new Date(order.createdAt)),
      order.person && order.person.name,
      p.name,
      p.quantity,
      (p.batch || '').replace('_Ref', ''),
      order.secondPerson && order.secondPerson.name,
    ]))
    .reduce((a, c) => a.concat(c), []);
  const headers = [
    'Invoice',
    'Date',
    'Customer',
    'Drug',
    'Quantity',
    'Batch',
    'Doctor',
  ];
  return csvGenerator(headers, data);
};

export const generateTotalStock = (stock) => {
  const data = stock.products
    .map((p, index) => [
      index, p.name,
      p.batch,
      p.quantity,
      (p.basePrice / 100).toFixed(2),
      (p.tax).toFixed(2),
      (p.netPrice).toFixed(2),
      p.expiry ? ddmmyyyy(new Date(p.epxiry)) : '',
    ]);
  const headers = [
    'Index',
    'Product Name',
    'Batch',
    'Quantity',
    'Rate',
    'Tax',
    'Net',
  ];
  return csvGenerator(headers, data);
};
