import fs from 'fs';
import fsExtra from 'fs-extra';
import path from 'path';

import {
  generateB2BR1,
  generateB2CLR1,
  generateCNDRR1,
  generateCNDURR1,
  generateEXPR1,
  generateATorATAJR1,
  generateHsnSummary,
  generateB2CSR1,
  generateB2BorB2BURR2,
  generateIMPGR2,
  generateIMPSR2,
  generateInvoiceSummary,
  generateDrugSummary,
  generatePrescribedDrugsSummary,
  generateTotalStock,
} from './reportHelper';

import Transaction from '../models/transaction';
import Product from '../models/product';
import CurrentState from '../models/currentState';

const REPORTS_ROOT = path.join(process.env.HOME, 'REPORTS');

export const writeGSTR1 = async (startDate, endDate) => {
  const orders = await Transaction.find({
    createdAt: {
      $gte: startDate,
      $lte: endDate,
    },
    type: {
      $eq: 'order',
    },
    subType: {
      $nin: ['discard'],
    },
  });

  const refundedOrders = await Transaction.find({
    refundedAt: {
      $gte: startDate,
      $lte: endDate,
    },
    type: {
      $eq: 'order',
    },
    subType: {
      $eq: 'refund',
    },
  });

  // Orders with registered users, with GSTIN
  const b2bOrders = orders.filter(order => order.person && order.person.gstin);

  // orders with unregistered users, Inter State and with value greater than 2.5 Lakhs
  const b2clOrders = orders.filter(order =>
    !(order.person && order.person.gstin)
    && (order.interState)
    && (order.total > 25000000));

  // Orders with unregistered users, rest
  const b2csOrders = orders.filter(order =>
    !(order.person && order.person.gstin)
    && (order.total < 25000000));

  // Refunds with Registered users
  const cndrOrders = refundedOrders.filter(order => order.person && order.person.gstin);

  // Refunds with unregistered users
  const cndurOrders = refundedOrders.filter(order => !(order.person && order.person.gstin));

  const directory = `GSTR1 -- ${(new Date(startDate)).toDateString()} -- ${(new Date(endDate)).toDateString()}`;
  try {
    if (fsExtra.ensureDir(path.join(REPORTS_ROOT, directory))) {
      await fsExtra.remove(path.join(REPORTS_ROOT, directory));
    }
    const dir = path.join(REPORTS_ROOT, directory, 'GSTR1');
    await fsExtra.mkdirp(dir);

    fs.writeFileSync(path.join(dir, 'b2b.csv'), generateB2BR1(b2bOrders));
    fs.writeFileSync(path.join(dir, 'b2ba.csv'), generateB2BR1(b2bOrders, true));
    fs.writeFileSync(path.join(dir, 'b2cl.csv'), generateB2CLR1(b2clOrders));
    fs.writeFileSync(path.join(dir, 'b2cla.csv'), generateB2CLR1(b2clOrders, true));
    fs.writeFileSync(path.join(dir, 'b2cs.csv'), generateB2CSR1(b2csOrders));
    fs.writeFileSync(path.join(dir, 'b2csa.csv'), generateB2CSR1(b2csOrders, true));
    fs.writeFileSync(path.join(dir, 'cndr.csv'), generateCNDRR1(cndrOrders));
    fs.writeFileSync(path.join(dir, 'cndra.csv'), generateCNDRR1(cndrOrders, true));
    fs.writeFileSync(path.join(dir, 'cndur.csv'), generateCNDURR1(cndurOrders, true));
    fs.writeFileSync(path.join(dir, 'cndura.csv'), generateCNDURR1(cndurOrders, true));
    fs.writeFileSync(path.join(dir, 'hsn.csv'), generateHsnSummary(orders));
    fs.writeFileSync(path.join(dir, 'exp.csv'), generateEXPR1());
    fs.writeFileSync(path.join(dir, 'expa.csv'), generateEXPR1(true));
    fs.writeFileSync(path.join(dir, 'ex.csv'), generateEXPR1(true));
    fs.writeFileSync(path.join(dir, 'expa.csv'), generateEXPR1(true));
    fs.writeFileSync(path.join(dir, 'at.csv'), generateATorATAJR1());
    fs.writeFileSync(path.join(dir, 'ata.csv'), generateATorATAJR1(true));
    fs.writeFileSync(path.join(dir, 'atadj.csv'), generateATorATAJR1());
    fs.writeFileSync(path.join(dir, 'atadja.csv'), generateATorATAJR1(true));
  } catch (err) {
    // eslint-disable-next-line
    console.log(err.message);
  }
};

export const writeGSTR2 = async (startDate, endDate) => {
  const stocks = await Transaction.find({
    createdAt: {
      $gte: startDate,
      $lte: endDate,
    },
    type: {
      $eq: 'stock',
    },
  });

  // Orders with registered users, with GSTIN
  const b2bStocks = stocks.filter(stock => stock.person && stock.person.gstin);

  // orders with unregistered users
  const b2burStocks = stocks.filter(stock =>
    !(stock.person && stock.person.gstin));

  const directory = `GSTR2 -- ${(new Date(startDate)).toDateString()} -- ${(new Date(endDate)).toDateString()}`;
  try {
    if (fsExtra.ensureDir(path.join(REPORTS_ROOT, directory))) {
      await fsExtra.remove(path.join(REPORTS_ROOT, directory));
    }
    const dir = path.join(path.join(REPORTS_ROOT, directory, 'GSTR2'));
    await fsExtra.mkdirp(dir);

    fs.writeFileSync(path.join(dir, 'b2b.csv'), generateB2BorB2BURR2(b2bStocks, true));
    fs.writeFileSync(path.join(dir, 'b2bur.csv'), generateB2BR1(b2burStocks));
    fs.writeFileSync(path.join(dir, 'impg.csv'), generateIMPGR2());
    fs.writeFileSync(path.join(dir, 'imps.csv'), generateIMPSR2());
    fs.writeFileSync(path.join(dir, 'at.csv'), generateATorATAJR1());
    fs.writeFileSync(path.join(dir, 'atadj.csv'), generateATorATAJR1());
    fs.writeFileSync(path.join(dir, 'hsnsum.csv'), generateHsnSummary(stocks));
  } catch (err) {
    // eslint-disable-next-line
    console.log(err.message);
  }
};

export const writeInvoiceSummary = async (startDate, endDate) => {
  const orders = await Transaction.find({
    createdAt: {
      $gte: startDate,
      $lte: endDate,
    },
    type: {
      $eq: 'order',
    },
    subType: {
      $nin: ['discard'],
    },
  });

  const stocks = await Transaction.find({
    createdAt: {
      $gte: startDate,
      $lte: endDate,
    },
    type: {
      $eq: 'stock',
    },
  });

  const directory = `Invoice Summary -- ${(new Date(startDate)).toDateString()} -- ${(new Date(endDate)).toDateString()}`;
  try {
    if (fsExtra.ensureDir(path.join(REPORTS_ROOT, directory))) {
      await fsExtra.remove(path.join(REPORTS_ROOT, directory));
    }
    const dir = path.join(path.join(REPORTS_ROOT, directory, 'Invoice Summary'));
    await fsExtra.mkdirp(dir);

    fs.writeFileSync(path.join(dir, 'outgoing.csv'), generateInvoiceSummary(orders));
    fs.writeFileSync(path.join(dir, 'incoming.csv'), generateInvoiceSummary(stocks));
  } catch (err) {
    throw err;
  }
};

export const writeScheduleDrugReports = async (startDate, endDate) => {
  const orders = await Transaction.find({
    createdAt: {
      $gte: startDate,
      $lte: endDate,
    },
    type: {
      $eq: 'order',
    },
    subType: {
      $nin: ['discard'],
    },
  });

  const products = await Product.find();

  const directory = `Schedule Drugs Report -- ${(new Date(startDate)).toDateString()} -- ${(new Date(endDate)).toDateString()}`;
  try {
    if (fsExtra.ensureDir(path.join(REPORTS_ROOT, directory))) {
      await fsExtra.remove(path.join(REPORTS_ROOT, directory));
    }
    const dir = path.join(path.join(REPORTS_ROOT, directory, 'Schedule Drugs'));
    await fsExtra.mkdirp(dir);

    ['h1', 'h', 'x', 'tb', 'narcotics'].forEach((drugType) => {
      fs.writeFileSync(path.join(dir, `${drugType}.csv`), generateDrugSummary(orders, products, drugType));
    });
  } catch (err) {
    throw err;
  }
};

export const writePrescribedDrugReports = async (startDate, endDate) => {
  const orders = await Transaction.find({
    createdAt: {
      $gte: startDate,
      $lte: endDate,
    },
    type: {
      $eq: 'order',
    },
    subType: {
      $nin: ['discard'],
    },
  });

  const products = await Product.find();

  const directory = `Prescribed Drugs Report -- ${(new Date(startDate)).toDateString()} -- ${(new Date(endDate)).toDateString()}`;
  try {
    if (fsExtra.ensureDir(path.join(REPORTS_ROOT, directory))) {
      await fsExtra.remove(path.join(REPORTS_ROOT, directory));
    }
    const dir = path.join(path.join(REPORTS_ROOT, directory, 'Prescribed Drugs'));
    await fsExtra.mkdirp(dir);

    fs.writeFileSync(path.join(dir, 'prescribed.csv'), generatePrescribedDrugsSummary(orders, products));
  } catch (err) {
    throw err;
  }
};

export const writeTotalStocks = async () => {
  const currentState = await CurrentState
    .findOne()
    .populate([{
      path: 'stocks.products.id',
      select: '_id name basePrice taxes tax discount netPrice costPrice details.prescription',
    }])
    .populate([{
      path: 'total.products.id',
      select: '_id minQuantity maxQuantity',
    }]);
  const directory = 'Stock Overview';
  try {
    if (fsExtra.ensureDir(path.join(REPORTS_ROOT, directory))) {
      await fsExtra.remove(path.join(REPORTS_ROOT, directory));
    }
    const dir = path.join(path.join(REPORTS_ROOT, directory, 'Stock Overview'));
    await fsExtra.mkdirp(dir);
    fs.writeFileSync(path.join(dir, 'stock.csv'), generateTotalStock(currentState.total));
  } catch (err) {
    throw err;
  }
};
