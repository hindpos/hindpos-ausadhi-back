import { modelToType, modelToCreateType, modelToUpdateType } from 'mongoose-to-graphql';
import CurrentState from '../models/currentState';

import { prepare } from '../helper';

const CurrentStateGQLSchema = modelToType(CurrentState);
const CurrentStateGQLInputSchema = `${modelToCreateType(CurrentState)}\n${modelToUpdateType(CurrentState)}`;

export const currentStateTypeDefs = {
  output: CurrentStateGQLSchema,
  input: CurrentStateGQLInputSchema,
  query: [
    'getCurrentState: CurrentState'],
  mutation: [
    'setCurrentState(currentState: CurrentStateCreate): CurrentState',
  ],
};

export const currentStateResolvers = {
  Query: {
    getCurrentState: async () => {
      const currentState = await CurrentState
        .findOne()
        .populate([{
          path: 'stocks.products.id',
          select: '_id name basePrice taxes tax discount netPrice costPrice details.prescription',
        }])
        .populate([{
          path: 'total.products.id',
          select: '_id minQuantity maxQuantity',
        }]);
      return prepare(currentState);
    },
  },
  Mutation: {
    setCurrentState: async (root, args) => {
      // pass it through a validator
      const currentState = CurrentState(args.currentState);
      try {
        let out = await CurrentState.findOneAndUpdate({}, currentState, {
          upsert: true,
          setDefaultsOnInsert: true,
        });
        out = await CurrentState.findOne();
        return prepare(out);
      } catch (err) {
        throw err;
      }
    },
  },
};
