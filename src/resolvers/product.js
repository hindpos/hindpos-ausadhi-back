import { modelToType, modelToCreateType, modelToUpdateType } from 'mongoose-to-graphql';
import Product from '../models/product';

import { prepare, dollarify } from '../helper';

const ProductGQLSchema = modelToType(Product);
const ProductGQLInputSchema = `${modelToCreateType(Product)}\n${modelToUpdateType(Product)}`;

export const productTypeDefs = {
  output: ProductGQLSchema,
  input: ProductGQLInputSchema,
  query: [
    'getProducts (product: ProductUpdate, limit: Int, skip: Int): [Product]'],
  mutation: [
    'createProduct(product: ProductCreate): Product',
    'updateProduct(filter: ProductUpdate, product: ProductCreate): Product',
    'deleteProduct(id: String): Product',
  ],
};

export const productResolvers = {
  Query: {
    getProducts: async (root, { product, limit, skip }) => {
      const dProduct = dollarify(product);
      const validProducts = await Product.find(dProduct)
        .sort({ createdAt: -1 })
        .skip(skip || 0)
        .limit(limit || Number.MAX_SAFE_INTEGER);
      return validProducts.map(prepare);
    },
  },
  Mutation: {
    createProduct: async (root, args) => {
      // pass it through a validator
      const product = Product(args.product);
      try {
        return prepare(await product.save());
      } catch (err) {
        throw err;
      }
    },
    updateProduct: async (root, { filter, product }) => {
      try {
        const dFilter = dollarify(filter);
        let out = await Product.findOneAndUpdate(dFilter, product, { upsert: true });
        out = await Product.findOne(dFilter);
        return prepare(out);
      } catch (err) {
        throw err;
      }
    },
    deleteProduct: async (root, { id }) => {
      try {
        await Product.find({ _id: id }).remove().exec();
        return {
          _id: '0',
        };
      } catch (err) {
        throw err;
      }
    },
  },
};
