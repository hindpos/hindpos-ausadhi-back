import {
  modelToType,
  modelToCreateType,
  modelToUpdateType,
} from 'mongoose-to-graphql';

import GlobalProduct from '../models/globalProduct';
import { prepare, dollarify } from '../helper';

const GlobalProductGQLSchema = modelToType(GlobalProduct);
const GlobalProductGQLInputSchema = `${modelToCreateType(GlobalProduct)}\n${modelToUpdateType(GlobalProduct)}`;

export const globalProductTypeDefs = {
  output: GlobalProductGQLSchema,
  input: GlobalProductGQLInputSchema,
  query: [
    'getGlobalProducts (product: GlobalProductUpdate, limit: Int, skip: Int): [GlobalProduct]',
  ],
  mutation: [
    'createGlobalProduct(product: GlobalProductCreate): GlobalProduct',
    'updateGlobalProduct(filter: GlobalProductUpdate, product: GlobalProductCreate): GlobalProduct',
  ],
};

export const globalProductResolvers = {
  Query: {
    getGlobalProducts: async (root, { product, limit, skip }) => {
      const dProduct = dollarify(product);
      const validProducts = await GlobalProduct.find(dProduct)
        .skip(skip || 0)
        .limit(limit || Number.MAX_SAFE_INTEGER);
      return validProducts.map(prepare);
    },
  },
  Mutation: {
    createGlobalProduct: async (root, args) => {
      // pass it through a validator
      const product = GlobalProduct(args.product);
      try {
        return prepare(await product.save());
      } catch (err) {
        throw err;
      }
    },
    updateGlobalProduct: async (root, { filter, product }) => {
      try {
        const dFilter = dollarify(filter);
        let out = await GlobalProduct.findOneAndUpdate(dFilter, product, { upsert: true });
        out = await GlobalProduct.findOne(dFilter);
        return prepare(out);
      } catch (err) {
        throw err;
      }
    },
  },
};
