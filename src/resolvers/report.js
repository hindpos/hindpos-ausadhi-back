import {
  writeGSTR1,
  writeGSTR2,
  writeInvoiceSummary,
  writeScheduleDrugReports,
  writePrescribedDrugReports,
  writeTotalStocks,
} from './reportWriter';

export const reportTypeDefs = {
  output: `input REPORT{
    r1: Boolean
    r2: Boolean
    invoice: Boolean
    schedule: Boolean
    prescription: Boolean
    stock: Boolean
    startDate: String
    endDate: String
  }
  type REPORTRESULT{
    success: Boolean
  }
  `,
  query: [
    'report (report: REPORT): REPORTRESULT'],
};

export const reportResolvers = {
  Query: {
    report: async (root, { report }) => {
      const { startDate, endDate } = report;
      try {
        if (report.r1) {
          await writeGSTR1(startDate, endDate);
        }
        if (report.r2) {
          await writeGSTR2(startDate, endDate);
        }
        if (report.invoice) {
          await writeInvoiceSummary(startDate, endDate);
        }
        if (report.schedule) {
          await writeScheduleDrugReports(startDate, endDate);
        }
        if (report.prescription) {
          await writePrescribedDrugReports(startDate, endDate);
        }
        if (report.stock) {
          await writeTotalStocks();
        }
      } catch (err) {
        // eslint-disable-next-line
        console.log(err.message);
        return { success: false };
      }
      return { success: true };
    },
  },
};
