import { modelToType, modelToCreateType, modelToUpdateType } from 'mongoose-to-graphql';
import Config from '../models/config';

import { prepare } from '../helper';

const ConfigGQLSchema = modelToType(Config);
const ConfigGQLInputSchema = `${modelToCreateType(Config)}\n${modelToUpdateType(Config)}`;

export const configTypeDefs = {
  output: ConfigGQLSchema,
  input: ConfigGQLInputSchema,
  query: [
    'getConfig: Config'],
  mutation: [
    'setConfig(config: ConfigCreate): Config'],
};

export const configResolvers = {
  Query: {
    getConfig: async () => {
      const config = await Config.findOne();
      return prepare(config);
    },
  },
  Mutation: {
    setConfig: async (root, args) => {
      // pass it through a validator
      const config = Config(args.config);
      try {
        let out = await Config.findOneAndUpdate({}, config, {
          upsert: true,
          setDefaultsOnInsert: true,
        });
        out = await Config.findOne();
        return prepare(out);
      } catch (err) {
        throw err;
      }
    },
  },
};
