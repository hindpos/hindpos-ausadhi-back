import { modelToType, modelToCreateType, modelToUpdateType } from 'mongoose-to-graphql';
import _ from 'lodash';
import mongoose from 'mongoose';

import Transaction from '../models/transaction';
import CurrentState from '../models/currentState';
/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

import { prepare, dollarify } from '../helper';

const TransactionGQLSchema = modelToType(Transaction);
const TransactionGQLInputSchema = `${modelToCreateType(Transaction)}\n${modelToUpdateType(Transaction)}`;

export const transactionTypeDefs = {
  output: TransactionGQLSchema,
  input: TransactionGQLInputSchema,
  query: [
    'getTransactions (transaction: TransactionUpdate, limit: Int, skip: Int): [Transaction]'],
  mutation: [
    'createTransaction(transaction: TransactionCreate): Transaction',
    'updateTransaction(filter: TransactionUpdate, transaction: TransactionCreate, isRefund: Int): Transaction',
  ],
};

const calculateAverage = (currentStock, keepAsPercent = false) => {
  const products = currentStock.products
    .map((product) => {
      const reqProduct = _.pick(product, ['_id', 'id', 'name', 'quantity', 'basePrice', 'netPrice', 'tax', 'discount', 'total', 'batch', 'costPrice', 'sellPrice', 'hsn']);
      reqProduct.discountVal = product.basePrice * (product.discount / 10000);
      reqProduct.taxVal = product.basePrice * (1 - (product.discount / 10000))
       * (product.tax / 10000);
      // eslint-disable-next-line no-param-reassign
      product.total = product.netPrice * product.quantity;
      if (!keepAsPercent) {
        reqProduct.discount = reqProduct.discountVal;
        reqProduct.tax = reqProduct.taxVal;
      }
      return [reqProduct];
    }).reduce((acc, nxt) => {
      const index = acc.map(p => p.id.toString()).indexOf(nxt[0].id.toString());
      if (index > -1) {
        const newProduct = _.cloneDeep(nxt[0]);
        // eslint-disable-next-line no-param-reassign
        nxt[0].total = nxt[0].netPrice * nxt[0].quantity;
        newProduct.quantity = acc[index].quantity + nxt[0].quantity;
        newProduct.basePrice = ((acc[index].basePrice * acc[index].quantity)
        + (nxt[0].basePrice * nxt[0].quantity)) / newProduct.quantity;
        newProduct.netPrice = ((acc[index].netPrice * acc[index].quantity)
        + (nxt[0].netPrice * nxt[0].quantity)) / newProduct.quantity;
        newProduct.tax = ((acc[index].taxVal * acc[index].quantity)
        + (nxt[0].taxVal * nxt[0].quantity)) / newProduct.quantity;
        newProduct.discount = ((acc[index].discountVal * acc[index].quantity)
        + (nxt[0].discountVal * nxt[0].quantity)) / newProduct.quantity;
        newProduct.total = acc[index].total + nxt[0].total;
        acc[index] = newProduct;
      } else {
        // eslint-disable-next-line no-param-reassign
        nxt[0].total = nxt[0].netPrice * nxt[0].quantity;
        acc.push(nxt[0]);
      }
      return acc;
    });
  const price = parseInt(products.map(p => p.basePrice * p.quantity)
    .reduce((acc, nxt) => acc + nxt), 10);
  const tax = parseInt(products.map(p => p.tax * p.quantity)
    .reduce((acc, nxt) => acc + nxt), 10);
  const discount = parseInt(products.map(p => p.discount * p.quantity)
    .reduce((acc, nxt) => acc + nxt), 10);
  const total = parseInt(products.map(p => p.netPrice * p.quantity)
    .reduce((acc, nxt) => acc + nxt), 10);
  const recalculatedCurrentStock = {
    products,
    price,
    discount,
    tax,
    total,
  };
  return recalculatedCurrentStock;
};

const createNewCurrentStateTotal = (newCurrentStateStocks) => {
  const tempCurrentStateStocks = JSON.parse(JSON.stringify(newCurrentStateStocks));
  const newCurrentStateTotalStocks = tempCurrentStateStocks.reduce((acc, nxt) => {
    acc.products = _.concat(acc.products, nxt.products);
    return acc;
  });
  const recalculatedTotal = calculateAverage(newCurrentStateTotalStocks);
  return recalculatedTotal;
};

const postStock = async (transaction, currentState) => {
  let newCurrentState = {
    stocks: [],
    total: null,
  };

  // Change newCurrentState.stocks
  if (currentState) {
    newCurrentState = _.pick(JSON.parse(JSON.stringify(currentState)), ['stocks', 'total']);
  }
  const currStockLast = _.pick(transaction, ['products', 'price', 'tax', 'total']);
  currStockLast.stock = transaction._id;
  newCurrentState.stocks.unshift(currStockLast);
  // Change newCurrentState.total
  newCurrentState.total = createNewCurrentStateTotal(newCurrentState.stocks);

  await CurrentState.findOneAndUpdate({}, newCurrentState, { upsert: true });
};

const postOrder = async (transaction, currentState) => {
  // Change newCurrentState.stocks
  const tempCurrentState = _.pick(JSON.parse(JSON.stringify(currentState)), ['stocks', 'total']);
  const soldProducts = transaction.products;
  const quantityArr = soldProducts.map(p => p.quantity);
  let quantitySum = quantityArr.reduce((acc, nxt) => acc + nxt, 0);
  const batchIDs = soldProducts.map(p => p._id.toString());
  let done = false;
  for (let s = 0; s < tempCurrentState.stocks.length; s += 1) {
    for (let p = 0; p < tempCurrentState.stocks[s].products.length; p += 1) {
      if (quantitySum === 0) {
        done = true;
        break;
      }
      const batchID = tempCurrentState.stocks[s].products[p]._id.toString();
      const idx = batchIDs.indexOf(batchID);
      if (idx > -1) {
        if (tempCurrentState.stocks[s].products[p].quantity < quantityArr[idx]) {
          throw new Error('Transaction has already been made somewhere else.');
        }
        tempCurrentState.stocks[s].products[p].quantity -= quantityArr[idx];
        quantitySum -= quantityArr[idx];
        quantityArr[idx] = 0;
      }
    }
    tempCurrentState.stocks[s].products = tempCurrentState.stocks[s].products
      .filter(p => p.quantity > 0);
    if (tempCurrentState.stocks[s].products.length > 0) {
      const tempStock = calculateAverage(tempCurrentState.stocks[s]);
      tempCurrentState.stocks[s].price = tempStock.price;
      tempCurrentState.stocks[s].discount = tempStock.discount;
      tempCurrentState.stocks[s].tax = tempStock.tax;
      tempCurrentState.stocks[s].total = tempStock.total;
    }
    if (done) {
      break;
    }
  }
  if (quantitySum > 0) {
    throw new Error('Transaction has already been made somewhere else.');
  }
  let newCurrentState = {};
  newCurrentState.stocks = tempCurrentState.stocks.filter(s => s.products.length > 0);
  // Change total
  if (newCurrentState.stocks.length > 0) {
    newCurrentState.total = createNewCurrentStateTotal(newCurrentState.stocks);
    await CurrentState.findOneAndUpdate({}, newCurrentState, { upsert: true });
  } else {
    newCurrentState = {};
    await CurrentState.findOneAndRemove();
  }
};

export const transactionResolvers = {
  Query: {
    getTransactions: async (root, { transaction, limit, skip }) => {
      const dTransaction = dollarify(transaction);
      const validTransactions = await Transaction.find(dTransaction)
        .sort({ createdAt: -1 })
        .skip(skip || 0)
        .limit(limit || Number.MAX_SAFE_INTEGER);
      return validTransactions.map(prepare);
    },
  },
  Mutation: {
    createTransaction: async (root, args) => {
      // pass it through a validator
      const transaction = Transaction(args.transaction);
      const currentState = await CurrentState.findOne().sort({ createdAt: -1 });
      const savedTransaction = await transaction.save();
      delete savedTransaction.details;
      try {
        if (transaction.type === 'stock') {
          await postStock(savedTransaction, currentState);
        } else {
          await postOrder(savedTransaction, currentState);
        }
        return prepare(savedTransaction);
      } catch (err) {
        await Transaction.findByIdAndRemove(savedTransaction._id);
        throw new Error('Transaction has already been made somewhere else.');
      }
    },
    updateTransaction: async (root, { filter, transaction, isRefund }) => {
      try {
        const dFilter = dollarify(filter);
        if (isRefund === 1) {
          const prev = await Transaction.findOne(dFilter);
          if (prev.subType === 'refund') {
            throw new Error('Refund has already been made somewhere else.');
          }
        }
        let out = await Transaction.findOneAndUpdate(dFilter, transaction, { upsert: true });
        out = await Transaction.findOne(dFilter);
        if (isRefund === 1) {
          const currentState = await CurrentState.findOne().sort({ createdAt: -1 });
          /* eslint-disable no-param-reassign */
          out.products = out.products
            .filter(p => 'refund' in p && p.refund.keep > 0);
          if (out.products.length > 0) {
            out.products.forEach((p) => {
              p.netPrice = p.costPrice;
              p.basePrice = p.costPrice / (1 + (p.tax / 10000));
              p.quantity = p.refund.keep;
              p._id = mongoose.Types.ObjectId();
              p.total = p.costPrice * p.refund.keep;
              p.batch = `${p.batch}_Ref`;
            });
            /* eslint-enable no-param-reassign */
            _.extend(out, calculateAverage(out, true));
            const reqOut = JSON.parse(JSON.stringify(out));
            delete reqOut.details;
            if (transaction.type === 'order') {
              await postStock(reqOut, currentState);
            }
          }
        }
        return prepare(out);
      } catch (err) {
        throw err;
      }
    },
  },
};
