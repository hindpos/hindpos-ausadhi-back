/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
import mapKeys from 'map-keys-deep-lodash';
import _ from 'lodash';

const emptyString = '';

export const prepare = (o) => {
  if (!o) {
    return o;
  }
  // eslint-disable-next-line no-param-reassign
  o._id = o._id.toString();
  return o;
};

export const flattenObject = (obj) => {
  if (!obj) {
    return obj;
  }
  const toReturn = {};
  Object.keys(obj).forEach((i) => {
    if (Object.keys(obj[i]).length > 0) {
      if ((typeof obj[i]) === 'object' && !(_.startsWith(Object.keys(obj[i])[0], '$'))) {
        const flatObject = flattenObject(obj[i]);
        Object.keys(flatObject).forEach((x) => {
          toReturn[`${i}.${x}`] = flatObject[x];
        });
      } else {
        toReturn[i] = obj[i];
      }
    }
  });
  return toReturn;
};

export const dollarify = (obj) => {
  if (!obj) {
    return obj;
  }
  const operators = ['lte', 'gte', 'lt', 'gt', 'eq', 'regex', 'options', 'nin'];
  const dollared = mapKeys(obj, (value, key) => {
    if (_.includes(operators, key)) {
      return `$${key}`;
    }
    return key;
  });
  const flat = flattenObject(dollared);
  // console.log(flat);
  return flat;
};

// Add typedefs, dependencies must go after typedefs on which they depend
export const addTypeDefs = (...args) => {
  const genSchema = (typeDefArray, property, furtherJoin) => typeDefArray
    .map((typedef) => {
      if (typeof typedef === 'object') {
        if (Object.prototype.hasOwnProperty.call(typedef, property)) {
          if (typedef[`${property}`]) {
            if (furtherJoin) {
              return typedef[`${property}`].join('\n');
            }
            return typedef[`${property}`];
          }
        }
      }
      return emptyString;
    })
    .join('\n');

  const scalarsSchema = genSchema(args, 'scalar', true);

  const querySchema = `type Query {
    ${genSchema(args, 'query', true)}
  }`;

  const mutationSchema = `type Mutation {
    ${genSchema(args, 'mutation', true)}
  }`;

  const inputSchema = genSchema(args, 'input');
  const outputSchema = genSchema(args, 'output');

  const typeDefs = `
  ${scalarsSchema}

  ${inputSchema}

  ${outputSchema}

  ${querySchema}

  ${mutationSchema}

  schema {
      query: Query
      mutation: Mutation
  }`;

  return typeDefs;
};
