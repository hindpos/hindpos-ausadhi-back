const randArrOf = (generator, number, casual) => {
  const iter = casual.integer(0, number);
  const arr = [];
  for (let i = 0; i < iter; i += 1) {
    arr.push(generator());
  }
  return arr;
};

module.exports = { randArrOf };
