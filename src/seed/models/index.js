// import CurrentState from '../../models/currentState';
// import Product from '../../models/product';
// import Transaction from '../../models/transaction';
// import User from '../../models/user';

// Raw Seeds
// import products from './productRaw';

// const casual = require('casual');
const mongoose = require('mongoose');

// require('./helperSchema')(casual);
// require('./user')(casual);
// require('./product')(casual);
// require('./transaction')(casual);
// require('./currentState')(casual);

mongoose.connect('mongodb://localhost:27017/posapp', async () => {
  await mongoose.connection.db.listCollections().toArray((err, collections) => {
    collections.forEach(async (collection) => {
      if (!collection.name.match('^globalproducts')) {
        console.log(`${collection.name} dropped`);
        await mongoose.connection.db.dropCollection(collection.name);
      }
    });
  });
  // let transaction;
  // let product;
  // let currentState;
  // let user;
  /* eslint-disable no-await-in-loop */
  // for (let i = 0; i < 50; i += 1) {
  // transaction = new Transaction(casual.myTransaction);
  // await transaction.save();

  // product = new Product(casual.myProduct);
  // await product.save();

  // await currentState.save();
  // currentState = new CurrentState(casual.myCurrentState);

  // user = new User(casual.myUser);
  // await user.save();
  // }
  // let prod;
  // for (let index = 0; index < products.length; index += 1) {
  //   prod = new Product(products[index]);
  //   await prod.save();
  // }
  // process.exit(0);
});

