// import casual from 'casual';
// const casual = require('casual');

/* eslint no-underscore-dangle: ["error", { "allow": ["_phone", "_email"] }] */

const _ = require('lodash');
const mongoose = require('mongoose');

const { randArrOf } = require('../utils');

const extendCasual = (casual) => {
  casual.define('myTax', () => {
    const data = {
      type: casual.random_element(['CGST', 'SGST', 'SAT', 'SET', 'ED', 'VAT', 'CD', 'ET']),
    };
    const percent = casual.integer(0, 100);
    const amount = casual.integer(0, 1000);
    _.extend(data, casual.random_element([{ percent }, { amount }]));
    return data;
  });
  casual.define('myContact', () => ({
    phones: randArrOf(casual._phone, 5, casual),
    emails: randArrOf(casual._email, 5, casual),
  }));
  casual.define('myAddress', () => ({
    lines: [casual.address1, casual.address2],
    landmark: casual.street,
    state: casual.state,
    pincode: casual.zip(7),
    country: casual.country,
  }));
  casual.define('myPerson', () => ({
    name: casual.full_name,
    address: casual.myAddress,
    contacts: casual.myContact,
    company: casual.company_name,
    gstin: `${casual.card_number}`,
    license: casual.name,
  }));
  casual.define('myPayment', () => {
    const data = {
      type: casual.random_element(['cash', 'debit', 'credit', 'cheque', 'dd', 'bank', 'mobile']),
      closed: casual.boolean,
      due: casual.integer(0, 1000),
      history: randArrOf(() => ({
        date: new Date(casual.unix_time),
        amount: casual.integer(1000),
      }), 10, casual),
    };
    if (data.history) {
      data.paid = data.history.reduce((acc, nxt) => acc + nxt.amount, 0);
    }
    if (data.closed) {
      data.due = 0;
    }
    return data;
  });
  casual.define('myPrice', () => {
    const global = casual.random_element([true, false]);
    const data = { global };
    if (!global) {
      const customOnGlobal = casual.random_element([{
        percent: casual.integer(-100, 200),
      }, {
        amount: casual.integer(100, 200000),
      }]);
      const custom = casual.integer(0, 20000);
      _.extend(data, casual.random_element([
        { custom },
        { customOnGlobal },
      ]));
    }
    return data;
  });
  casual.define('myDiscount', () => casual.random_element([
    { percent: casual.integer(0, 10000) },
    { amount: casual.integer(0, 10000) },
  ]));
  casual.define('myProductTransaction', () => ({
    batch: casual.rgb_hex,
    id: new mongoose.mongo.ObjectId(),
    name: casual.name,
    expiry: new Date(casual.unix_time),
    basePrice: casual.integer(0, 100000),
    discount: casual.myDiscount,
    quantity: casual.integer(0, 100),
    netPrice: casual.integer(0, 100000),
    taxes: randArrOf(() => casual.myTax, 5, casual),
    tax: casual.integer(0, 10000),
    total: casual.integer(0, 100000),
    refund: {
      refund: casual.integer(0, 50),
      discard: casual.integer(0, 50),
      keep: casual.integer(0, 50),
    },
  }));
};

module.exports = extendCasual;
