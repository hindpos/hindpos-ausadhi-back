const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/posapp', async () => {
  await mongoose.connection.db.listCollections().toArray((err, collections) => {
    collections.forEach(async (collection) => {
      if (collection.name.match('transactions' || 'transactioncounters' || 'currentstates')) {
        console.log(`${collection.name} dropped`);
        await mongoose.connection.db.dropCollection(collection.name);
      }
    });
  });
});

