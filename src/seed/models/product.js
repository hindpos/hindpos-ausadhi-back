const _ = require('lodash');
const { randArrOf } = require('../utils');
const mongoose = require('mongoose');

const extendCasual = (casual) => {
  casual.define('myProduct', () => {
    const data = {
      name: casual.name,
      price: casual.myPrice,
      taxes: randArrOf(() => casual.myTax, 5, casual),
      discount: casual.myDiscount,
      details: casual.card_data,
      user: new mongoose.mongo.ObjectId(),
      username: casual.username.replace(/[^A-Za-z]/, ''),
    };
    data.minQuantity = casual.integer(0, 10);
    data.maxQuantity = casual.integer(1000, 2000);
    return data;
  });
};

module.exports = extendCasual;
