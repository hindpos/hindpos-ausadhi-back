
const extendCasual = (casual) => {
  casual.define('myUser', () => ({
    name: casual.name,
    username: casual.username.replace(/[^A-Za-z]/, ''),
    password: casual.password,
    department: casual.random_element(['Sales', 'Admin', 'Registry']),
    contacts: casual.myContact,
    address: casual.myAddress,
    permissions: casual.random_element(['Admin', 'Non-admin', 'Others']),
  }));
};

module.exports = extendCasual;
