const { randArrOf } = require('../utils');
const mongoose = require('mongoose');

const extendCasual = (casual) => {
  casual.define('myCurrentStock', () => ({
    stock: new mongoose.mongo.ObjectId(),
    products: randArrOf(() => casual.myProductTransaction, 10, casual),
    basePrice: casual.integer(100, 1000000),
    tax: casual.integer(100, 100000),
    netPrice: casual.integer(100, 1000000),
    total: casual.integer(100, 100000),
  }));
  casual.define('myCurrentState', () => ({
    stockList: randArrOf(() => casual.myCurrentStock, 10, casual),
    total: casual.myCurrentStock,
  }));
};

module.exports = extendCasual;
