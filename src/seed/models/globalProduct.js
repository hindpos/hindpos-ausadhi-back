import { MongoClient } from 'mongodb';
import mongoose from 'mongoose';
import asyncLoop from 'node-async-loop';

import GlobalProduct from '../../models/globalProduct';

/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

const url = 'mongodb://localhost:27017';
const dbUrl = 'mongodb://localhost:27017/posapp';

const dbName = 'posapp';
MongoClient.connect(url, async (err, client) => {
  mongoose.connect(dbUrl, async () => {
    const db = client.db(dbName);
    const collection = db.collection('rawProducts');
    collection.find({}).toArray(async (err2, docs) => {
      asyncLoop(docs, async (doc, next) => {
        const doc2 = doc;
        delete doc2._id;
        const glob = new GlobalProduct(doc2);
        await glob.save();
        // console.log(doc);
        next();
      });
    });
  });
});
