import Promise from 'bluebird';

import mongoose from 'mongoose';
import Mongod from 'mongod';
import path from 'path';

const mongoDbURI = 'mongodb://localhost:27017';
const database = 'posapp';

mongoose.Promise = Promise;

const server = new Mongod({
  port: 27017,
  dbpath: path.join(process.env.HOME, '.mongo'),
});

const mongooseConnect = () => {
  server.open((err) => {
    if (err) {
      console.log(err.message);
    } else {
      mongoose.connect(`${mongoDbURI}/${database}`);
    }
  });
};

const mongooseClose = () => {
  mongoose.connection.close();
  server.close((err) => {
    console.log(err.message);
  });
};

module.exports = { mongooseConnect, mongooseClose };
