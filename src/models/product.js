import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

const ProductSchema = mongoose.Schema(
  {
    name: {
      $type: String,
      required: true,
      trim: true,
    },
    basePrice: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
    netPrice: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
    tax: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
    discount: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
    minQuantity: {
      $type: Number,
      set: v => Math.round(v),
      min: 0,
    },
    maxQuantity: {
      $type: Number,
      set: v => Math.round(v),
      min: 0,
    },
    details: {
      $type: mongoose.Schema.Types.Mixed,
    },
    user: {
      $type: mongoose.Schema.Types.ObjectId, // buyer in case of stock, and seller in case of order
      required: true,
    // Validation not finalised yet!
    },
    username: String,
  },
  { typeKey: '$type' },
);

ProductSchema.plugin(timestamps);
const Product = mongoose.model('Product', ProductSchema);

export default Product;
