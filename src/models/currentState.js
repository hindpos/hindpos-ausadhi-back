import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

import { ProductTransactionSchema } from './helperSchema';

const CurrentStockSchema = mongoose.Schema({
  _id: false,
  stock: {
    $type: mongoose.Schema.Types.ObjectId,
    // required: true,
  },
  products: {
    $type: [ProductTransactionSchema],
    required: true,
  },
  price: {
    $type: Number,
    set: v => Math.round(v),
    min: 0,
    // required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  tax: {
    $type: Number,
    set: v => Math.round(v),
    min: 0,
    // required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  total: {
    $type: Number,
    set: v => Math.round(v),
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

const CurrentStateSchema = mongoose.Schema({
  stocks: {
    $type: [CurrentStockSchema],
    required: true,
  },
  total: {
    $type: CurrentStockSchema,
    required: true,
  },
}, { typeKey: '$type' });

CurrentStateSchema.plugin(timestamps);

const CurrentState = mongoose.model('CurrentState', CurrentStateSchema);

export default CurrentState;
