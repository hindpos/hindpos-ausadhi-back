import mongoose from 'mongoose';
import { isEmpty } from '../utils';

const ContactSchema = mongoose.Schema({
  _id: false,
  phones: {
    $type: [String],
  },
  emails: {
    $type: [String],
    // validate: [val => checkAll(val, validator.isEmail), 'permissions should be alphanumeric'],
  },
}, { typeKey: '$type' });

const AddressSchema = mongoose.Schema({
  _id: false,
  lines: {
    $type: [String],
  },
  landmark: {
    $type: String,
  },
  state: {
    $type: String,
  },
  city: {
    $type: String,
  },
  pincode: {
    $type: String,
  },
  country: {
    $type: String,
  },
}, { typeKey: '$type' });

const PersonSchema = mongoose.Schema({
  _id: false,
  name: {
    $type: String,
    trim: true,
  },
  address: AddressSchema,
  contacts: ContactSchema,
  company: String,
  gstin: String,
  license: String,
  gender: Boolean,
  birthDate: Date,
  age: {
    $type: Number,
    set: v => Math.round(v),
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
}, { typeKey: '$type' });

const PaymentSchema = mongoose.Schema({
  _id: false,
  type: {
    $type: String,
    required: true,
    enum: ['cash', 'debit', 'credit', 'cheque', 'dd', 'bank', 'mobile', 'card'],
  },
  closed: Boolean,
  paid: {
    $type: Number,
    set: v => Math.round(v),
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  due: {
    $type: Number,
    set: v => Math.round(v),
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  history: [{
    _id: false,
    date: Date,
    amount: {
      $type: Number,
      set: v => Math.round(v),
      required: true,
      validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
    },
    user: mongoose.Schema.Types.ObjectId,
    username: String,
  }],
  details: {
    $type: mongoose.Schema.Types.Mixed, // a json converted to String
  },
}, { typeKey: '$type' });

const PriceSchema = mongoose.Schema({
  _id: false,
  global: {
    $type: Boolean,
  },
  customOnGlobal: {
    percent: {
      $type: Number,
      set: v => Math.round(v),
      min: -100,
    },
    amount: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
  },
  custom: {
    $type: Number,
    set: v => Math.round(v),
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

PriceSchema.pre('save', function priceValidator(next) {
  if ((this.global) ^
  (!isEmpty(this.customOnGlobal.percent)) ^
  (!isEmpty(this.customOnGlobal.amount)) ^
  (!isEmpty(this.custom))) {
    next();
  } else {
    next(new Error('Price can be one of the following types : (global, customOnGlobal, custom)'));
  }
});

const DiscountSchema = mongoose.Schema({
  _id: false,
  percent: {
    // Percent * 100
    $type: Number,
    set: v => Math.round(v),
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  amount: {
    $type: Number,
    set: v => Math.round(v),
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

DiscountSchema.pre('save', function discountValidor(next) {
  if ((!isEmpty(this.percent)) ^ (!isEmpty(this.amount))) {
    next();
  } else {
    // console.log(this);
    next(new Error('Discount should either be a percentage or an amount'));
  }
});

const CounterSchema = mongoose.Schema({
  _id: { type: String, required: true },
  order: { type: Number, default: 0 },
  stock: { type: Number, default: 0 },
  credit: { type: Number, default: 0 },
  debit: { type: Number, default: 0 },
});

const ProductTransactionSchema = mongoose.Schema({
  // _id: false,
  batch: String,
  id: {
    $type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
    required: true,
  },
  name: String,
  expiry: Date,
  basePrice: {
    $type: Number,
    set: v => Math.round(v),
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  hsn: String,
  discount: {
    // tax * 100
    $type: Number,
    set: v => Math.round(v),
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  quantity: {
    $type: Number,
    set: v => Math.round(v),
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  netPrice: {
    $type: Number,
    set: v => Math.round(v),
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  tax: {
    // tax * 100
    $type: Number,
    set: v => Math.round(v),
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  total: {
    $type: Number,
    set: v => Math.round(v),
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  costPrice: {
    $type: Number,
    set: v => Math.round(v),
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  sellPrice: {
    $type: Number,
    set: v => Math.round(v),
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  refund: {
    refund: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
    },
    discard: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
    },
    keep: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
    },
    amount: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
    },
  },
}, { typeKey: '$type' });

export {
  AddressSchema,
  ContactSchema,
  PersonSchema,
  PaymentSchema,
  PriceSchema,
  DiscountSchema,
  CounterSchema,
  ProductTransactionSchema,
};
