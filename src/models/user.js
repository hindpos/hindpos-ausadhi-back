import timestamps from 'mongoose-timestamp';
import mongoose from 'mongoose';
import validator from 'validator';

import { ContactSchema, AddressSchema } from './helperSchema';

const UserSchema = mongoose.Schema({
  name: {
    $type: String,
    required: true,
    trim: true,
  },
  username: {
    $type: String,
    required: true,
    trim: true,
    unique: true,
    validate: [validator.isAlphanumeric, 'username should be alphanumeric'],
  },
  password: String,
  department: {
    $type: String,
    trim: true,
  },
  contacts: {
    $type: ContactSchema,
  },
  address: {
    $type: AddressSchema,
  },
  birthDate: Date,
  gender: Boolean,
  admin: {
    $type: Boolean,
    default: false,
  },
  permissions: {
    $type: [String],
    // required: true,
  },

}, { typeKey: '$type' });

UserSchema.plugin(timestamps);

const User = mongoose.model('User', UserSchema);

export default User;
