import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

import { AddressSchema } from './helperSchema';

const ConfigSchema = mongoose.Schema({
  _id: false,
  smallSidebar: {
    $type: Boolean,
    default: false,
  },
  autoBill: {
    $type: Boolean,
    default: false,
  },
  expiryWarn: {
    $type: Number,
    set: v => Math.round(v),
    default: 7,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  printType: {
    $type: String,
    default: 'Thermal',
  },
  target: {
    profit: {
      $type: Number,
      set: v => Math.round(v),
      default: 1000,
      validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
    },
    revenue: {
      $type: Number,
      set: v => Math.round(v),
      default: 10000,
      validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
    },
    order: {
      $type: Number,
      set: v => Math.round(v),
      default: 100,
      validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
    },
  },
  store: {
    id: {
      $type: String,
      required: true,
    },
    name: {
      $type: String,
      required: true,
    },
    owner: {
      $type: String,
    },
    gstin: {
      $type: String,
    },
    license: {
      $type: String,
    },
    phone: {
      $type: String,
    },
  },
  address: AddressSchema,
  numCopies: {
    $type: Number,
    set: v => Math.round(v),
    default: 1,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
}, { typeKey: '$type' });

ConfigSchema.plugin(timestamps);
const Config = mongoose.model('Config', ConfigSchema);

export default Config;
