import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

import { PaymentSchema, PersonSchema, CounterSchema, ProductTransactionSchema } from './helperSchema';

const TransactionCounter = mongoose.model('TransactionCounter', CounterSchema);

const TransactionSchema = mongoose.Schema({
  num: { // an incremental num
    $type: Number,
    set: v => Math.round(v),
    // required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  isInterState: {
    $type: Boolean,
    default: false,
  },
  type: {
    $type: String,
    required: true,
    enum: [
      'order',
      'stock',
      'credit',
      'debit',
    ],
  },
  subType: {
    $type: String,
    // required: true,
    enum: [
      'create',
      'refund',
      'discard',
      'profit',
      'loss',
    ],
  },
  user: {
    $type: mongoose.Schema.Types.ObjectId, // buyer in case of stock, and seller in case of order
    required: true,
  },
  username: String,
  person: PersonSchema,
  secondPerson: PersonSchema,
  products: {
    $type: [ProductTransactionSchema], // A list of productTransaction object which contains
    required: true,
  },
  price: { // exclusive of all taxes and discount
    $type: Number,
    set: v => Math.round(v),
    // We will store the amount of money in paise
    // Comparison on float data$types are bad.
    // required: true,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  tax: { // In paise
    $type: Number,
    set: v => Math.round(v),
    // required: true,
    min: 0,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  discount: { // In paise
    $type: Number,
    set: v => Math.round(v),
    // required: true,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  total: { // inclusive of all taxes and discount in paise
    $type: Number,
    set: v => Math.round(v),
    required: true,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  refundAmount: {
    $type: Number,
    set: v => Math.round(v),
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  refundDate: Date,
  receiptNum: String,
  payment: {
    $type: PaymentSchema,
  },
  description: {
    $type: String,
  },
  details: {
    $type: mongoose.Schema.Types.Mixed, // A json coneverted to String
  },
}, { typeKey: '$type' });

TransactionSchema.pre('save', function transactionValidator(next) {
  switch (this.type) {
    case 'stock':
      TransactionCounter.findByIdAndUpdate({ _id: 'entityId' }, { $inc: { stock: 1 } }, { new: true, upsert: true }).then((count) => {
        this.num = count.stock;
        next();
      });
      break;
    case 'order':
      TransactionCounter.findByIdAndUpdate({ _id: 'entityId' }, { $inc: { order: 1 } }, { new: true, upsert: true }).then((count) => {
        this.num = count.order;
        next();
      });
      break;
    case 'debit':
      TransactionCounter.findByIdAndUpdate({ _id: 'entityId' }, { $inc: { debit: 1 } }, { new: true, upsert: true }).then((count) => {
        this.num = count.debit;
        next();
      });
      break;
    case 'credit':
      TransactionCounter.findByIdAndUpdate({ _id: 'entityId' }, { $inc: { credit: 1 } }, { new: true, upsert: true }).then((count) => {
        this.num = count.credit;
        next();
      });
      break;
    default:
      break;
  }
});

TransactionSchema.plugin(timestamps);
const Transaction = mongoose.model('Transaction', TransactionSchema);

export default Transaction;
