#! /usr/bin/env node
// babel
import 'babel-core/register';
import 'babel-polyfill';
import cors from 'cors';

// module imports
import { merge } from 'lodash';
import { makeExecutableSchema } from 'graphql-tools';
import express from 'express';
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';

// internal imports
import { typeDefs, resolvers } from 'mongoose-to-graphql';

// file imports
import { addTypeDefs } from './helper';
import { currentStateTypeDefs, currentStateResolvers } from './resolvers/currentState';
import { productTypeDefs, productResolvers } from './resolvers/product';
import { globalProductTypeDefs, globalProductResolvers } from './resolvers/globalProduct';
import { transactionTypeDefs, transactionResolvers } from './resolvers/transaction';
import { userTypeDefs, userResolvers } from './resolvers/user';
import { configTypeDefs, configResolvers } from './resolvers/config';
import { reportTypeDefs, reportResolvers } from './resolvers/report';
import { mongooseConnect, mongooseClose } from './mongoose';

export default {
  mongooseClose,
};

const PORT = 3001;
const URL = 'http://localhost';

// Connecting to mongodb
mongooseConnect();

// Joining all typedefs
const mergedTypeDefs = addTypeDefs(
  typeDefs,
  currentStateTypeDefs,
  productTypeDefs,
  globalProductTypeDefs,
  transactionTypeDefs,
  userTypeDefs,
  configTypeDefs,
  reportTypeDefs,
);

// console.log(typeDefs);

// Merging all resolvers, see merge function from lodash
const mergedResolvers = merge(
  resolvers,
  currentStateResolvers,
  productResolvers,
  globalProductResolvers,
  transactionResolvers,
  userResolvers,
  configResolvers,
  reportResolvers,
);

// console.dir(resolvers, { depth: 2, colors: true });

const schema = makeExecutableSchema({
  typeDefs: mergedTypeDefs,
  resolvers: mergedResolvers,
});

const app = express();

app.use(cors());
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

const homePath = '/graphiql';

app.use(homePath, graphiqlExpress({
  endpointURL: '/graphql',
}));

app.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Visit ${URL}:${PORT}${homePath}`);
});
